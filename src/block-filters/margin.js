import classnames from 'classnames';

import UkMarginControl from '../common-inspector-controls/margin';

import marginClasses from '../common-functions/margin';

//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls 
} = wp.blockEditor;

const { 
	PanelBody, 
	SelectControl 
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkMarginOnBlocks = [
	'uk-gb/uk-grid-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-item-block',
	'uk-gb/uk-button',
	'uk-gb/uk-divider',
	'uk-gb/uk-heading',
	'uk-gb/uk-split-item-left',
	'uk-gb/uk-split-item-right',
	'uk-gb/uk-split-item',
];


// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addUkMarginAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkMarginOnBlocks.includes( name ) ) {
		return settings;
	}

	//Set null widths
	// Use Lodash's assign to gracefully handle if attributes are undefined
	settings.attributes = Object.assign( settings.attributes, {
		ukBasicMargin: {
			type: 'string',
			default: ''
		},
		ukTopMargin: {
			type: 'string',
			default: ''
		},
		ukBottomMargin: {
			type: 'string',
			default: ''
		},
		ukLeftMargin: {
			type: 'string',
			default: ''
		},
		ukRightMargin: {
			type: 'string',
			default: ''
		},
		ukMarginRemove: {
			type: 'bool',
			default: false
		},
		ukMarginRemoveTop: {
			type: 'bool',
			default: false
		},
		ukMarginRemoveBottom: {
			type: 'bool',
			default: false
		},
		ukMarginRemoveLeft: {
			type: 'bool',
			default: false
		},
		ukMarginRemoveRight: {
			type: 'bool',
			default: false
		},
		ukMarginRemoveVertical: {
			type: 'bool',
			default: false
		},
		ukMarginRemoveHorizontal: {
			type: 'bool',
			default: false
		}
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-margin', addUkMarginAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const ukMarginInspector = createHigherOrderComponent( ( BlockEdit ) => {

		

	return ( props ) => {
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkMarginOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukBasicMargin,
			ukTopMargin,
			ukBottomMargin,
			ukLeftMargin,
			ukRightMargin,
			ukMarginRemove,
			ukMarginRemoveTop,
			ukMarginRemoveBottom,
			ukMarginRemoveLeft,
			ukMarginRemoveRight,
			ukMarginRemoveVertical,
			ukMarginRemoveHorizontal,

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<UkMarginControl

						ukBasicMargin={props.attributes.ukBasicMargin}
						ukTopMargin={props.attributes.ukTopMargin}
						ukBottomMargin={props.attributes.ukBottomMargin}
						ukLeftMargin={props.attributes.ukLeftMargin}
						ukRightMargin={props.attributes.ukRightMargin}
						onChange = {updateAttribute}

					>
					</UkMarginControl>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'ukMarginInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-margin-control', ukMarginInspector );


/**Add Margin to Block List Block 
 *
 * For the following only 
 * Grid Item
*/
const editorMarginClasses = createHigherOrderComponent((BlockListBlock) => {
  	return props => {
  		// Do nothing if it's another block than our defined ones.
		if ( ! props.name === 'uk-gb/grid-item' ) {
			return <BlockListBlock { ...props } />
		}
		

		//Margin atts
		const { 
			ukBasicMargin,
			ukTopMargin,
			ukBottomMargin,
			ukLeftMargin,
			ukRightMargin,
			ukMarginRemove,
			ukMarginRemoveTop,
			ukMarginRemoveBottom,
			ukMarginRemoveLeft,
			ukMarginRemoveRight,
			ukMarginRemoveVertical,
			ukMarginRemoveHorizontal,

		} = props.attributes;

		const marginAttributes = [
			'ukBasicMargin',
			'ukTopMargin',
			'ukBottomMargin',
			'ukLeftMargin',
			'ukRightMargin',
			'ukMarginRemove',
			'ukMarginRemoveTop',
			'ukMarginRemoveBottom',
			'ukMarginRemoveLeft',
			'ukMarginRemoveRight',
			'ukMarginRemoveVertical',
			'ukMarginRemoveHorizontal',
		] 

		// Check for margin atts
		const hasMarginAttribute = marginAttributes.some(attributeName => props.attributes[attributeName]);

		// If we have margin atts
		if (hasMarginAttribute) {
			// Margin Classes
			var ukMarginClasses = marginClasses(props.attributes);

    		return <BlockListBlock { ...props } className={`${ukMarginClasses && ukMarginClasses}`} />

    	} else {
			
			return <BlockListBlock { ...props } />

    	}
  	}
}, 'editorMarginClasses')
wp.hooks.addFilter('editor.BlockListBlock', 'uk-gb/uk-margin-block-list-block', editorMarginClasses)

