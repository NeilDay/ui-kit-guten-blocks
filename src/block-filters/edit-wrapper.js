// UK GB WRAPPER - HANDLES WRAPPING THE BLOCK IN THE EDITOR IF IT HAS WIDTH, BACKGROUND, PADDING MARGIN ANIMATION ETC.


import classnames from 'classnames';

import UkWrapper from './wrapper.js';

//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const allowedBlocks = [
	//'uk-gb/uk-section-block',
];

const ukGbWrapper = createHigherOrderComponent( ( BlockListBlock ) => {
    return ( props ) => {
		if ( ! allowedBlocks.includes( props.name ) ) {
			return <BlockListBlock {...props} />
		}
		console.log('edit wrapper')
        return (
			<UkWrapper
				attributes={props.attributes}
				doWidth={false}
			>

				<BlockListBlock 
					{...props}
				/>			
			</UkWrapper>
    	)

    };
}, 'ukGbWrapper' );

//wp.hooks.addFilter( 'editor.BlockListBlock', 'uk-gb/uk-gb-wrapper', ukGbWrapper );
