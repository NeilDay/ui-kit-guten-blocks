import classnames from 'classnames';

import assign from 'lodash/assign';


//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls 
} = wp.blockEditor;

const { 
	PanelBody, 
	SelectControl,
	ToggleControl,
	TextControl,
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkHeightOnBlocks = [
    'uk-gb/uk-grid-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-item-block',
	'uk-gb/uk-split-bg',
	'uk-gb/uk-split-item',
];

const ukHeightOptions = [
	{ label: 'auto', 			value: '' },
	{ label: 'Small - 150px', 			value: 'uk-min-height-small' },
	{ label: 'Medium - 300px', 			value: 'uk-min-height-medium' },
	{ label: 'Large - 450px', 			value: 'uk-min-height-large' },
	{ label: 'Viewport Height', 		value: 'uk-min-height-viewport' },
	{ label: 'Custom', 					value: 'uk-min-height-custom' },
];

// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addUkHeightAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkHeightOnBlocks.includes( name ) ) {
		return settings;
	}

	settings.attributes = Object.assign( settings.attributes, {

		ukHeightDesk: {
			type: 'string',
			default: 'auto'
		},
		ukHeightDeskCustom: {
			type: 'number',
			default: 500
		},
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-height', addUkHeightAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const ukHeightInspector = createHigherOrderComponent( ( BlockEdit ) => {

		

	return ( props ) => {
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkHeightOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukDeskHeight,
			ukHeightDeskCustom

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'UK Height' ) }
			            initialOpen={ false }
					>
						<SelectControl
							label={ __( 'Sets the MINIMUM height only' ) }
							value={ props.attributes.ukHeightDesk }
							options={ ukHeightOptions }
							onChange={ ( value ) => {
								props.setAttributes( {
									ukHeightDesk: value,
								} );
							} }
						/>
                        { props.attributes.ukHeightDesk === 'uk-min-height-custom'  ? (
                            <Fragment>
                               <TextControl
       								label={__("Custom Minimum Height")}
       								value={props.attributes.ukHeightDeskCustom}
       								onChange={ ( value ) => {
										props.setAttributes( {
											ukHeightDeskCustom: parseInt(value),
										} );
									} }
       							/>
                            </Fragment>
                        ) : '' }
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'ukHeightInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-height-control', ukHeightInspector );


const editorHeightClasses = createHigherOrderComponent((BlockListBlock) => {
  	return props => {
  		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkHeightOnBlocks.includes( props.name ) ) {
			return <BlockListBlock { ...props } />
		}
		
		const { 
			ukHeightDesk

		} = props.attributes;

		// Atts required for bg

		var heightAttributes = [
			'ukHeightDesk'
		] 

		const hasHeightAttribute = heightAttributes.some(attributeName => props.attributes[attributeName]);

		// If we have bg atts - add the bg + width
		if (hasHeightAttribute) {
    		return <BlockListBlock { ...props } className={ 'has-uk-height '+`${props.attributes.ukHeightDesk}` } />;

    	} else {
			
			return <BlockListBlock { ...props } />

    	}
  	}
}, 'editorHeightClasses')
//wp.hooks.addFilter('editor.BlockListBlock', 'my-plugin/with-uk-height', editorHeightClasses)

function addUkHeightSave( extraProps, blockType, attributes ) {

	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkHeightOnBlocks.includes( blockType.name ) ) {
		return extraProps;
	}
	
	var heightAttributes = [
		'ukViewHeight'
	] 

	const hasHeightAttribute = heightAttributes.some(attributeName => attributes[attributeName]);
	
	// If we have bg atts - add the bg + width
	if (hasHeightAttribute) {


		return lodash.assign( extraProps, { 
			className: 'has-uk-height '+attributes.ukHeightDesk,
			'uk-height-viewport': (ukHeightDesk === 'viewport' ? true : undefined),
			'style': (ukHeightDesk === 'custom' ? attributes.ukHeightDeskCustom+'px' : undefined) 
		} );

	} else {
		
		return extraProps

	}

}
 
//wp.hooks.addFilter('blocks.getSaveContent.extraProps','my-plugin/add-uk-height',addUkHeightSave);


