import classnames from 'classnames';

import VerticalAlignmentButtons from '../common-inspector-controls/vertical_align';

import assign from 'lodash/assign';

import icons from '../icons';


//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls 
} = wp.blockEditor;

const { 
	PanelBody, 
	SelectControl,
	Dashicon, 
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkVerticalOnBlocks = [
	'uk-gb/uk-item-block',
	'uk-gb/uk-split-bg'
];

const deskValignItems = [ 
	{ id: 1, value: 'uk-flex-stretch', icon:<Dashicon icon="image-flip-vertical" />},
	{ id: 1, value: 'uk-flex-top', icon:icons.alignTop},
	{ id: 2, value: 'uk-flex-middle', icon:icons.alignCenter},
	{ id: 3, value: 'uk-flex-bottom', icon:icons.alignBottom}
];

// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addUkWidthAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkVerticalOnBlocks.includes( name ) ) {
		return settings;
	}

	settings.attributes = Object.assign( settings.attributes, {
		ukDeskV_align: {
			type: 'string',
			default: 'uk-flex-stretch'
		},
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-v-align', addUkWidthAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const ukWidthInspector = createHigherOrderComponent( ( BlockEdit ) => {

		

	return ( props ) => {
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkVerticalOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukDeskV_align

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'Content Alignmnet' ) }
						initialOpen={ true }
					>
						<VerticalAlignmentButtons
							verticalAlignment={props.attributes.ukDeskV_align}
							items={deskValignItems}
							onClick={ ( value ) => props.setAttributes({ ukDeskV_align: value })}
						/>
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'ukWidthInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-width-control', ukWidthInspector );


const editorWidthClasses = createHigherOrderComponent((BlockListBlock) => {
  	return props => {
  		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkVerticalOnBlocks.includes( props.name ) ) {
			return <BlockListBlock { ...props } />
		}
		
		const { 
			align,
			ukDeskWidth,
			ukTabWidth,
			ukMobWidth

		} = props.attributes;

		// Atts required for bg

		if (props.attributes.align === 'full') {
			
			var widthAttributes = [
				'nowidth',
				'alignfull'
			] 

		} else if (props.attributes.align === 'wide') {
			
			var widthAttributes = [
				'nowidth',
				'alignwide'
			] 

		} else {
			
			var widthAttributes = [
				'ukDeskWidth',
				'ukTabWidth',
				'ukMobWidth'
			] 

		}	

		const hasWidthAttribute = widthAttributes.some(attributeName => props.attributes[attributeName]);

		// If we have bg atts - add the bg + width
		if (hasWidthAttribute) {
			// Width classes
			const ukWidthClasses = classnames('has-uk-width', props.className, {
				[props.attributes.ukDeskWidth] : props.attributes.ukDeskWidth,
				[props.attributes.ukTabWidth] : props.attributes.ukTabWidth,
				[props.attributes.ukMobWidth] : props.attributes.ukMobWidth
			})

    		return <BlockListBlock { ...props } className={ukWidthClasses} />

    	} else {
			
			return <BlockListBlock { ...props } />

    	}
  	}
}, 'editorWidthClasses')
//wp.hooks.addFilter('editor.BlockListBlock', 'my-plugin/with-custom-class-name', editorWidthClasses)

function addUkWidthSave( extraProps, blockType, attributes ) {

	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkVerticalOnBlocks.includes( blockType.name ) ) {
		return extraProps;
	}

	if (attributes.align === 'full') {
		var widthAttributes = [
			'nowidth',
			'alignfull'
		] 

	} else if (attributes.align === 'wide') {
		var widthAttributes = [
			'nowidth',
			'alignwide'
		] 

	} else {
		var widthAttributes = [
			'ukDeskWidth',
			'ukTabWidth',
			'ukMobWidth'
		] 

	}	

	const hasWidthAttribute = widthAttributes.some(attributeName => attributes[attributeName]);

	// If we have bg atts - add the bg + width
	if (hasWidthAttribute) {
		// Width classes
		const ukWidthClasses = classnames(extraProps.className, {
			[attributes.ukDeskWidth] : attributes.ukDeskWidth,
			[attributes.ukTabWidth] : attributes.ukTabWidth,
			[attributes.ukMobWidth] : attributes.ukMobWidth
		})

		return lodash.assign( extraProps, { className: ukWidthClasses } );

	} else {
		
		return extraProps

	}

}
 
//wp.hooks.addFilter('blocks.getSaveContent.extraProps','my-plugin/add-background-color-style',addUkWidthSave);


