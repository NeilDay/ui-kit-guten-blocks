import classnames from 'classnames';

//import UkWidthControl from '../common-inspector-controls/width';
import get from 'lodash/get';
import hexToRGBA from '../common-inspector-controls/hex-to-rgba';
import AdvancedColorControl from '../common-inspector-controls/advanced-color-control-palette';

import assign from 'lodash/assign';


//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls,
	PanelColorSettings,
	ColorPalette,
} = wp.blockEditor;

const { 
	PanelBody,
	ToggleControl,
	RangeControl,
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const {
	withSelect,
} = wp.data;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableTextShadowOnBlocks = [
	'uk-gb/uk-heading',
];


// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addTextShadowAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableTextShadowOnBlocks.includes( name ) ) {
		return settings;
	}

	//Set null widths
	// Use Lodash's assign to gracefully handle if attributes are undefined
	settings.attributes = Object.assign( settings.attributes, {
		ukgb_TextShadow: {
			type: 'bool',
			default: false
		},
		ukgb_TextShadowX: {
			type: 'number',
			default: '0'
		},
		ukgb_TextShadowY: {
			type: 'number',
			default: '0'
		},
		ukgb_TextShadowBlur: {
			type: 'number',
			default: '0'
		},
		ukgb_TextShadowColor: {
			type: 'string',
			default: '0'
		},
		ukgb_TextColor: {
			type: 'string',
			default: null
		},
		ukgb_TextOpacity: {
			type: 'number',
			default: '1'
		}
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/text-shadow', addTextShadowAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const textShadowInspector = createHigherOrderComponent( ( BlockEdit ) => {

	

	return ( props ) => {

		
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableTextShadowOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 
			ukgb_TextShadow,
			ukgb_TextShadowX,
			ukgb_TextShadowy,
			ukgb_TextShadowBlur,
			ukgb_TextShadowColor,
			ukgb_TextShadowOpacity,
			ukgb_TextColor,
			ukgb_TextOpacity,

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'UI Kit Text Styles' ) }
						initialOpen={ false }
					>
						<div className="uk-gb-control-section uk-gb-toggle-control">
							<AdvancedColorControl
	        					label={ __( 'Text Color' ) }
	        					opacityValue={'1'}
	        					colorValue={ ( props.attributes.ukgb_TextColor ? props.attributes.ukgb_TextColor : '' ) }
	        					colorDefault={ '' }
	        					onColorChange={ ( value ) => {

										props.setAttributes( {
										    ukgb_TextColor: value,
										} );

	        						}
									
	        					}
	        					onOpacityChange={ ( value ) => {

										props.setAttributes( {
	    								    ukgb_TextOpacity: value,
	    								} );
	        						}
	    								
	        					}
	        				/>
							<ToggleControl
								label={__("Enable Text Shadow")}
								checked={!!props.attributes.ukgb_TextShadow}
								onChange={() => {
									props.setAttributes({ ukgb_TextShadow: !ukgb_TextShadow });
								}}
							/>
							<RangeControl
								label={ __( 'X Axis' ) }
								value={ props.attributes.ukgb_TextShadowX }
								min={ -40 }
								max={ 40 }
								step={ 1 }
								onChange={ ( value ) => {
									props.setAttributes({ ukgb_TextShadowX: value });
								} }
							/>
							<RangeControl
								label={ __( 'X Axis' ) }
								value={ props.attributes.ukgb_TextShadowY }
								min={ -40 }
								max={ 40 }
								step={ 1 }
								onChange={(value) => {
									props.setAttributes({ ukgb_TextShadowY: value });
								}}
							/>
							<RangeControl
								label={ __( 'Blur' ) }
								value={ props.attributes.ukgb_TextShadowBlur }
								min={ 0 }
								max={ 100 }
								step={ 1 }
								onChange={(value) => {
									props.setAttributes({ ukgb_TextShadowBlur: value });
								}}
							/>
							<RangeControl
								label={ __( 'Opacity' ) }
								value={ props.attributes.ukgb_TextShadowOpacity }
								min={ 0 }
								max={ 1 }
								step={ 0.05 }
								onChange={(value) => {
									props.setAttributes({ ukgb_TextShadowOpacity: value });
								}}
							/>
							<AdvancedColorControl
	        					label={ __( 'Color' ) }
	        					opacityValue={'1'}
	        					colorValue={ ( props.attributes.ukgb_TextShadowColor ? props.attributes.ukgb_TextShadowColor : '' ) }
	        					colorDefault={ '' }
	        					onColorChange={ ( value ) => {

										props.setAttributes( {
										    ukgb_TextShadowColor: value,
										} );

	        						}
									
	        					}
	        					onOpacityChange={ ( value ) => {

										props.setAttributes( {
	    								    ukgb_TextShadowOpacity: value,
	    								} );
	        						}
	    								
	        					}
	        				/>
						</div>
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'textShadowInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/text-shadow-control', textShadowInspector );


const editorTextShadowClasses = createHigherOrderComponent((BlockListBlock) => {
  	return props => {
  		// Do nothing if it's another block than our defined ones.
		if ( ! enableTextShadowOnBlocks.includes( props.name ) ) {
			return <BlockListBlock { ...props } />
		}
		
		const { 
			ukgb_TextShadow,
			ukgb_TextShadowX,
			ukgb_TextShadowy,
			ukgb_TextShadowBlur,
			ukgb_TextShadowColor,
			ukgb_TextShadowOpacity,
			ukgb_TextColor,
			ukgb_TextOpacity

		} = props.attributes;

		var textShadowAttributes = [
			'ukgb_TextShadow',
			'ukgb_TextShadowX',
			'ukgb_TextShadowy',
			'ukgb_TextShadowBlur',
			'ukgb_TextShadowColor',
			'ukgb_TextShadowOpacity',
			'ukgb_TextColor',
		]

		const hasTextShadowAttribute = textShadowAttributes.some(attributeName => props.attributes[attributeName]);

		// If we have bg atts - add the bg + width
		if (props.attributes.ukgb_TextShadow === true) {

			var backgroundRGBA = hexToRGBA( props.attributes.ukgb_TextShadowColor, props.attributes.ukgb_TextShadowOpacity )	

    		return 	<BlockListBlock { ...props } 
    					style={{ 
    						textShadow: props.attributes.ukgb_TextShadow ? backgroundRGBA : '',
    						color: props.attributes.ukgb_TextColor ? props.attributes.ukgb_TextColor : '',

    					}}
    				/>

    	} else {
			
			return <BlockListBlock { ...props } />

    	}
  	}

}, 'editorTextShadowClasses')
//wp.hooks.addFilter('editor.BlockListBlock', 'my-plugin/with-text-shadow', editorTextShadowClasses)

function addTextShadowSave( extraProps, blockType, attributes ) {

	// Do nothing if it's another block than our defined ones.
	if ( ! enableTextShadowOnBlocks.includes( blockType.name ) ) {
		return extraProps;
	}	

	var textShadowAttributes = [
		'ukgb_TextShadow',
		'ukgb_TextShadowX',
		'ukgb_TextShadowy',
		'ukgb_TextShadowBlur',
		'ukgb_TextShadowColor',
		'ukgb_TextShadowOpacity',
		'ukgb_TextColor,'
	]

	const hasTextShadowAttribute = textShadowAttributes.some(attributeName => attributes[attributeName]);

		

	var textShadowFinal = null;

	// If we have bg atts - add the bg + width
	if (attributes.ukgb_TextShadow == true) {
		// Width classes
		var backgroundRGBA = hexToRGBA( attributes.ukgb_TextShadowColor, attributes.ukgb_TextShadowOpacity )
		
		var textShadowFinal = `${attributes.ukgb_TextShadowX}px ${attributes.ukgb_TextShadowY}px ${attributes.ukgb_TextShadowBlur}px ${attributes.ukgb_TextShadowColor}`

	} 

	return lodash.assign( extraProps, { style: { 
		textShadow: textShadowFinal,
		color: attributes.ukgb_TextColor
	} } );

}
 
wp.hooks.addFilter('blocks.getSaveContent.extraProps','my-plugin/add-text-shadow',addTextShadowSave);


