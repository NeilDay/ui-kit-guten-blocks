import get from 'lodash/get';
import map from 'lodash/map';

import icons from '../icons';

import hexToRGBA from '../common-inspector-controls/hex-to-rgba';
import AdvancedColorControl from '../common-inspector-controls/advanced-color-control-palette';
/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;

const { 
	addFilter
} = wp.hooks;

const { 
	createHigherOrderComponent 
} = wp.compose;

const { 
	Component,
	Fragment 
} = wp.element;

const { 
	InspectorControls,
	MediaUpload, 
} = wp.blockEditor;

const { 
	PanelRow,
	PanelBody, 
	ToggleControl,
	SelectControl,
	RangeControl,
	Button,
	ButtonGroup,
	Dashicon
} = wp.components;



// Available spacing control options
const ukGradientOptions = [
	{ label: 'top', value: 'to bottom', icon:<Dashicon icon="arrow-down-alt" /> },
	{ label: 'left', value: 'to right', icon:<Dashicon icon="arrow-right-alt" /> },
	{ label: 'top-left', value: '135deg', icon:<Dashicon icon="arrow-right-alt" /> },
	{ label: 'top-right', value: '305deg', icon:<Dashicon icon="arrow-right-alt" /> },
];

/**
 * Add spacing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */

const enableUkBackgroundOnBlocks = [
	'uk-gb/uk-section-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-item-block',
	'uk-gb/uk-split-item-left',
	'uk-gb/uk-split-item-right',
	'uk-gb/uk-split-item',
];

const addUkBackgroundAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.

	if ( ! enableUkBackgroundOnBlocks.includes( name ) ) {


		return settings;
	}

	const newSettings = {
        ...settings,
        attributes: {
            ...settings.attributes, // spread in old attributes so we don't lose them!
            background_image_style: {
            	type: 'string',
            	default: 'cover'
            },
    		has_parallax: {
				type: 'bool',
				default: false
			},
			parallaxOffset: {
				type: 'number',
				default: '200'
			},
			background_image: {
				type: 'string',
				default: null
			},
			backgroundColor_1: {
				type: 'string',
				default: null
			},
			backgroundColor_1_opacity: {
				type: 'number',
				default: '1'
			},
			backgroundColor_2: {
				type: 'string',
				default: null
			},
			backgroundColor_2_opacity: {
				type: 'number',
				default: '1'
			},
			gradientDirection: {
				type: 'string',
				default: 'to bottom'
			},
			bg_final: {
				type: 'string',
				default: null
			}
        }
    }

    return newSettings;

};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-background', addUkBackgroundAttribute );

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const withUkBackground = createHigherOrderComponent( ( BlockEdit ) => {
	
	return ( props ) => {
		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkBackgroundOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 
			background_image_style,
			has_parallax,
			parallaxOffset,
			background_image,
			backgroundColor_1,
			backgroundColor_1_opacity,
			backgroundColor_2,
			backgroundColor_2_opacity,
			gradientDirection,
			bg_final

		} = props.attributes;

		if ( props.attributes.backgroundColor_1){
			
			var bg_1 = hexToRGBA( props.attributes.backgroundColor_1, props.attributes.backgroundColor_1_opacity )

			if ( props.attributes.backgroundColor_2 ) {

				var bg_2 = hexToRGBA( props.attributes.backgroundColor_2, props.attributes.backgroundColor_2_opacity )

		   		props.setAttributes( {
                    bg_final: 'linear-gradient('+gradientDirection+','+bg_1+','+bg_2+')',
                } );

			} else {

				props.setAttributes( {
                    bg_final: bg_1,
                } );

			}
				
		} else {

			props.setAttributes( {
                bg_final: null,
            } );

		}

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'UI Kit Background' ) }
						initialOpen={ false }
					>
					{ ! props.attributes.background_image ? (
                            <MediaUpload
                                onSelect={ ( image ) => {
                                    props.setAttributes( {
                                        background_image: image.url,
                                    } );
                                } }
                                allowedTypes={ [ 'image' ] }
                                value={ props.attributes.background_image }
                                render={ ( { open } ) => (
                                    <Button onClick={ open } isPrimary>
                                        { __( 'Select image' ) }
                                    </Button>
                                ) }
                            />
                        ) : '' }
                        { props.attributes.background_image  ? (
                            <Fragment>
                                <img src={props.attributes.background_image}></img>
                                <p>
	                                <a
	                                    href="#"
	                                    onClick={ ( e ) => {
	                                        props.setAttributes( {
	                                            background_image: '',
	                                        } );
	                                        e.preventDefault();
	                                    } }
	                                >
	                                    { __( 'Remove image' ) }
	                                </a>
	                            </p>
                                <MediaUpload
                                    onSelect={ ( image ) => {
                                        props.setAttributes( {
                                            background_image: image.url,
                                        } );
                                    } }
                                    allowedTypes={ [ 'image' ] }
                                    value={ props.attributes.background_image }
                                    render={ ( { open } ) => (
                                        <Button onClick={ open } isPrimary>
                                            { __( 'Change image' ) }
                                        </Button>
                                    ) }
                                />
                            </Fragment>
                        ) : '' }
                        { props.attributes.background_image  ? (
							<div className="uk-gb-control-section uk-gb-toggle-control">
								<SelectControl
									label={__("Choose Background Image Style")}
									value={ props.attributes.background_image_style }
							        options={ [
							            { label: 'Cover', value: 'Cover' },
							            { label: 'Fixed', value: 'Fixed' },
							            { label: 'Parallax', value: 'Parallax' },
							        ] }
							        onChange={ ( value ) => { 
										props.setAttributes( {
										    background_image_style: value,
										} );
							        } }
								/>
								{ props.attributes.background_image_style === 'Parallax' && (
		                        	//If we have 1 colour output second color picker
		                            <RangeControl
	                                    label="Parallax Offset"
	                                    value={ props.attributes.parallaxOffset }
                                        onChange={ ( value ) => { 
                                			props.setAttributes( {
                                			    parallaxOffset: value,
                                			} );
                                        } }
	                                    min={ -1000 }
	                                    max={ 1000 }
	                                />
		                        )}
							</div>
                        ) : '' }
                        <div className="uk-gb-background-colour-control">
							<AdvancedColorControl
	        					label={ __( 'Background Color' ) }
	        					opacityValue={props.attributes.backgroundColor_1_opacity}
	        					colorValue={ ( props.attributes.backgroundColor_1 ? props.attributes.backgroundColor_1 : '' ) }
	        					colorDefault={ '' }
	        					onColorChange={ ( value ) => {

										props.setAttributes( {
										    backgroundColor_1: value,
										} );

	        						}
									
	        					}
	        					onOpacityChange={ ( value ) => {

										props.setAttributes( {
	    								    backgroundColor_1_opacity: value,
	    								} );
	        						}
	    								
	        					}
	        				/>
	                        { props.attributes.backgroundColor_1  ? (
	                        	//If we have 1 colour output second color picker
	                            <AdvancedColorControl
	            					label={ __( 'Color 2' ) }
	            					opacityValue={props.attributes.backgroundColor_2_opacity}
	            					colorValue={ ( props.attributes.backgroundColor_2 ? props.attributes.backgroundColor_2 : '' ) }
	            					colorDefault={ '' }
	            					onColorChange={ ( value ) => {

											props.setAttributes( {
											    backgroundColor_2: value,
											} );

	            						}
	    								
	            					}
	            					onOpacityChange={ ( value ) => {

											props.setAttributes( {
		    								    backgroundColor_2_opacity: value,
		    								} );

	            						}
		    								
	            					}
	            				/>
	                        ) : '' }
	                        { props.attributes.backgroundColor_2  ? (

	                        	//If we habe 2 colours output gradent buttons
	                        	<div className="uk-gb-control-section uk-gb-graditent-control">
	                        		<h2>Gradient Direction</h2>
									<ButtonGroup
		                				className="uk-gb-button-group uk-gb-gradient-buttons"
		                	      	>
		                				{ map( ukGradientOptions, ( { label, value, icon } ) => (
		                					<Button
		                						key={ label }
		                						className={label}
		                						isDefault
		                						isPrimary={ props.attributes.gradientDirection === value }
		                	                    isToggled={ props.attributes.gradientDirection === value }
		                						onClick={ () => {
													
													props.setAttributes({ gradientDirection: value });


		                						}
														
		                						}
		                					>
		                						{ icon }
		                					</Button>
		                				) ) }
		                	      	</ButtonGroup>
	                        	</div>
		                    ) : '' }
                        </div>
                        
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withUkBackground' );

addFilter( 'editor.BlockEdit', 'uk-gb/with-uk-background-control', withUkBackground );
