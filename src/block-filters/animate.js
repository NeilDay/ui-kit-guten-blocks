const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, SelectControl, RangeControl, ToggleControl } = wp.components;
const { addFilter } = wp.hooks;
const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkAnimationOnBlocks = [
	'uk-gb/uk-section-block',
	'uk-gb/uk-grid-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-item-block',
	'uk-gb/uk-divider',
	'uk-gb/uk-heading',
	'uk-gb/uk-button',
	'ui-kit-gallery',
	'uk-gb/uk-split-item-left',
	'uk-gb/uk-split-item-right',
	'uk-gb/uk-split-item',
];

// Available spacing control options
const ukAnimationOptions = [
	{label: 'none', 				value: 'none'},
	{label: 'scale-up', 			value: 'uk-animation-scale-up'},
	{label: 'scale-down', 			value: 'uk-animation-scale-down'},
	{label: 'slide-top',			value: 'uk-animation-slide-top'},
	{label: 'slide-bottom', 		value: 'uk-animation-slide-bottom'},
	{label: 'slide-left', 			value: 'uk-animation-slide-left'},
	{label: 'slide-right', 			value: 'uk-animation-slide-right'},
	{label: 'slide-top-small', 		value: 'uk-animation-slide-top-small'},
	{label: 'slide-bottom-small', 	value: 'uk-animation-slide-bottom-small'},
	{label: 'slide-left-small', 	value: 'uk-animation-slide-left-small'},
	{label: 'slide-right-small', 	value: 'uk-animation-slide-right-small'},
	{label: 'slide-top-medium', 	value: 'uk-animation-slide-top-medium'},
	{label: 'slide-bottom-medium', 	value: 'uk-animation-slide-bottom-medium'},
	{label: 'slide-left-medium', 	value: 'uk-animation-slide-left-medium'},
	{label: 'slide-right-medium', 	value: 'uk-animation-slide-right-medium'},
	{label: 'kenburns', 			value: 'uk-animation-kenburns'},
	{label: 'shake', 				value: 'uk-animation-shake'},
	{label: 'stroke', 				value: 'uk-animation-stroke'},
];

/**
 * Add spacing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addUkAnimationAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkAnimationOnBlocks.includes( name ) ) {
		return settings;
	}

	// Use Lodash's assign to gracefully handle if attributes are undefined
	settings.attributes = Object.assign( settings.attributes, {
		ukAnimation: {
			type: 'string',
			default: ukAnimationOptions[ 0 ].value,
		},
		ukAnimationDelay: {
			type: 'number',
			default: '0',
		},
		ukAnimationTopOffset: {
			type: 'number',
			default: '0',
		},
		ukAnimationRepeat: {
			type: 'bool',
			default: false,
		}

	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-animation', addUkAnimationAttribute );

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const withUkAnimation = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {
		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkAnimationOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukAnimation,
			ukAnimationDelay,
			ukAnimationTopOffset,
			ukAnimationRepeat

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'UI Kit Animations Filter' ) }
						initialOpen={ false }
					>
						<SelectControl
							label={ __( 'Animation Type' ) }
							value={ ukAnimation }
							options={ ukAnimationOptions }
							onChange={ ( selectedAnimationOption ) => {
								props.setAttributes( {
									ukAnimation: selectedAnimationOption,
								} );
							} }
						/>
						<RangeControl
							className="uk-gb-opacity-value"
							label={ 'Delay' }
							value={ ukAnimationDelay }
							onChange={ (value) => {
								props.setAttributes( {

									ukAnimationDelay: value

								} )

							} }
							min={ 0 }
							max={ 1500 }
							step={ 100 }
						/>
						<RangeControl
							className="uk-gb-opacity-value"
							label={ 'Offset Top' }
							value={ ukAnimationTopOffset }
							onChange={ (value) => {
								props.setAttributes( {

									ukAnimationTopOffset: value

								} )

							} }
							min={ -1000 }
							max={ 1000 }
							step={ 100 }
						/>
						<ToggleControl
							label={__("Repeat Animation")}
							checked={ !! ukAnimationRepeat}
							onChange={() => {
								props.setAttributes({ ukAnimationRepeat: !ukAnimationRepeat });
							}}
						/>
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withUkAnimation' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-width-control',withUkAnimation );


const withUkAnimationName = createHigherOrderComponent( ( BlockListBlock ) => {
    return ( props ) => {
        if(props.attributes.ukAnimation) {
        	Object.assign( props, { 
				...props.attributes.ukAnimation ? `uk-scrollspy: cls:${props.attributes.ukAnimation}` : null
			} );
            return <BlockListBlock { ...props } />
        } else {
            return <BlockListBlock {...props} />
        }

    };
}, 'withUkAnimationName' );

wp.hooks.addFilter( 'editor.BlockListBlock', 'uk-gb/attribute/uk-animation-name', withUkAnimationName );

/**
 * Add margin style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
const addUkAnimationsExtraProps = ( saveElementProps, blockType, attributes ) => {

	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkAnimationOnBlocks.includes( blockType.name ) ) {

		return saveElementProps;

	}
	if (attributes.ukAnimation === 'none') {
		
		return saveElementProps;

	}

	var amimation_string = 'cls:'+attributes.ukAnimation+'; delay:'+attributes.ukAnimationDelay+'; offset-top:'+attributes.ukAnimationTopOffset;

	if (attributes.ukAnimationRepeat === true) {
		
		amimation_string += '; repeat: true';

	}

	Object.assign( saveElementProps, { 
		...attributes.ukAnimationDelay,
		...attributes.ukAnimationTopOffset,
		...attributes.ukAnimation && { 'uk-scrollspy' : amimation_string}

	} );

	return saveElementProps;
};

addFilter( 'blocks.getSaveContent.extraProps', 'uk-gb/get-save-content/uk-animation-props', addUkAnimationsExtraProps );
