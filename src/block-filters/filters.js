// Filters to add UI Kit stuff to existing blocks -https://github.com/liip/extend-block-example-wp-plugin

import './width.js';
import './padding.js';
import './margin.js';
import './height.js';
import './background.js';
import './animate.js';
import './box-shadow.js';
import './text-shadow.js';


/*
 * Wrap all ACF Blocks in Disabled block to disable all links in edit view
 */
const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
import { Disabled } from '@wordpress/components';


const withDisabledCompontent =  createHigherOrderComponent( ( BlockEdit ) => {
  return ( props ) => {

      // only wrap block when it's a acf block in preview mode
      if (
        ! props.name.startsWith('acf/')
        || 'preview' != props.attributes.mode
      ) {
        return <BlockEdit { ...props } />;
      };


      return (
          <Fragment>
            <Disabled>
              <BlockEdit { ...props } />
            </Disabled>
          </Fragment>
      );
  };
}, "withInspectorControl" );

wp.hooks.addFilter( 'editor.BlockEdit', 'derweili/disabled-acf-blocks', withDisabledCompontent );


//import './edit-wrapper.js';
//import './save-wrapper.js';


//COMPONENTS
//import './wrapper.js';

wp.blocks.registerBlockStyle( 'core/heading', {
    name: 'custom-button-style',
    label: 'My Button Style'
} );

wp.blocks.registerBlockStyle( 'uk-gb/uk-section-block', {
    name: 'uk-section-xsmall',
    label: 'X Small'
} );
wp.blocks.registerBlockStyle( 'uk-gb/uk-section-block', {
    name: 'uk-section-small',
    label: 'Small'
} );
wp.blocks.registerBlockStyle( 'uk-gb/uk-section-block', {
    name: 'uk-section-large',
    label: 'Large'
} );
wp.blocks.registerBlockStyle( 'uk-gb/uk-section-block', {
    name: 'uk-section-xlarge',
    label: 'X Large'
} );


