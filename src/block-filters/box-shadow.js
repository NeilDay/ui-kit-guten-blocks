import classnames from 'classnames';

//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls 
} = wp.blockEditor;

const { 
	PanelBody, 
	SelectControl 
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkBoxShadowOnBlocks = [
	'uk-gb/uk-item-block',
	'uk-gb/uk-grid-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-split-item-left',
	'uk-gb/uk-split-item-right',
	'uk-gb/uk-split-item',
];

const ukBoxShadowOptions = [
	{label : 'none', 	value : ''},
	{label : 'small', 	value : 'uk-box-shadow-small'},
	{label : 'medium', 	value : 'uk-box-shadow-medium'},
	{label : 'large', 	value : 'uk-box-shadow-large'},
	{label : 'xlarge', 	value : 'uk-box-shadow-xlarge'},
]


// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addUkBoxShadowAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkBoxShadowOnBlocks.includes( name ) ) {
		return settings;
	}

	//Set null widths
	// Use Lodash's assign to gracefully handle if attributes are undefined
	settings.attributes = Object.assign( settings.attributes, {
		ukBoxShadow: {
			type: 'string',
			default: ''
		},
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-box-shadow', addUkBoxShadowAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const ukBoxShadowInspector = createHigherOrderComponent( ( BlockEdit ) => {

		

	return ( props ) => {
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkBoxShadowOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukBoxShadow,

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'UK Box Shadow' ) }
			            initialOpen={ false }
					>
						<SelectControl
							value={ props.attributes.ukBoxShadow }
							options={ ukBoxShadowOptions }
							onChange={ ( value ) => {
								props.setAttributes( {
									ukBoxShadow: value,
								} );
							} }
						/>
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'ukBoxShadowInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-box-shadow-control', ukBoxShadowInspector );

