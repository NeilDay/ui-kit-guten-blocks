// UK GB WRAPPER - HANDLES WRAPPING THE BLOCK IN THE EDITOR IF IT HAS WIDTH, BACKGROUND, PADDING MARGIN ANIMATION ETC.


import classnames from 'classnames';

import UkWrapper from './wrapper.js';

//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const allowedBlocks = [
	'uk-gb/uk-section-block',
	'uk-gb/uk-grid-block',
	'uk-gb/uk-grid-item'
];

/**
 * Add background wrapper to block.
 *
 */

const saveElementUkBackground = ( element, blockType, attributes ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! allowedBlocks.includes( blockType.name ) ) {
		return element;
	}
    return (
		<UkWrapper
			attributes={attributes}
			doWidth={true}
		>

			{element}

		</UkWrapper>
	)
};

//addFilter( 'blocks.getSaveElement', 'uk-gb/get-save-element-uk-background', saveElementUkBackground );
