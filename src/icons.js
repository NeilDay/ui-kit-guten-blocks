const icons = {};

icons.upload = <svg width='20px' height='20px' viewBox='0 0 100 100' xmlns='http://www.w3.org/2000/svg'>
    <path d='m77.945 91.453h-72.371c-3.3711 0-5.5742-2.3633-5.5742-5.2422v-55.719c0-3.457 2.1172-6.0703 5.5742-6.0703h44.453v11.051l-38.98-0.003906v45.008h60.977v-17.133l11.988-0.007812v22.875c0 2.8789-2.7812 5.2422-6.0664 5.2422z'
    />
    <path d='m16.543 75.48l23.25-22.324 10.441 9.7773 11.234-14.766 5.5039 10.539 0.039063 16.773z'
    />
    <path d='m28.047 52.992c-3.168 0-5.7422-2.5742-5.7422-5.7461 0-3.1758 2.5742-5.75 5.7422-5.75 3.1797 0 5.7539 2.5742 5.7539 5.75 0 3.1719-2.5742 5.7461-5.7539 5.7461z'
    />
    <path d='m84.043 30.492v22.02h-12.059l-0.015625-22.02h-15.852l21.941-21.945 21.941 21.945z'
    />
</svg>;

icons.alignBottom = <svg width='20px' height='20px' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
    <path fill="none" d="M0 0h24v24H0V0z" />
	<path d="M16 13h-3V3h-2v10H8l4 4 4-4zM4 19v2h16v-2H4z" />
</svg>;

icons.alignCenter = <svg width='20px' height='20px' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
    <path fill="none" d="M0 0h24v24H0V0z" />
	<path d="M8 19h3v4h2v-4h3l-4-4-4 4zm8-14h-3V1h-2v4H8l4 4 4-4zM4 11v2h16v-2H4z"/>
</svg>;

icons.alignBottom = <svg width='20px' height='20px' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
    <path fill="none" d="M0 0h24v24H0V0z" />
	<path d="M16 13h-3V3h-2v10H8l4 4 4-4zM4 19v2h16v-2H4z" />
</svg>;


icons.alignTop = <svg width='20px' height='20px' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
    <path fill="none" d="M0 0h24v24H0V0z" />
	<path d="M8 11h3v10h2V11h3l-4-4-4 4zM4 3v2h16V3H4z" />
</svg>;

icons.alignStretch = <svg width='20px' height='20px' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
    <path fill="none" d="M0 0h24v24H0V0z" />
    <path d="M3.3,2.5v1.7h13.3V2.5H3.3z M3.3,15.8v1.7h13.3v-1.7H3.3z M13.3,12.5h-2.5c0-2,0-3.9,0.1-5.9c-0.6,0-1.1,0-1.7,0.1
    c0,1.9,0,3.9-0.1,5.8H6.7l3.3,3.3L13.3,12.5z M10,4.2L6.7,7.5h2.5c0,1.9,0,3.9,0.1,5.8c0.6,0,1.1,0,1.7,0.1c0-2,0-3.9-0.1-5.9h2.5
    L10,4.2z"/>
</svg>;


export default icons;