import map from 'lodash/map';

import classnames from 'classnames';

import Background from '../components/background';

import marginClasses from '../common-functions/margin';
import paddingClasses from '../common-functions/padding';
import boxShadowClasses from '../common-functions/box-shadow';
import heightClasses from '../common-functions/height';

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

import icons from '../icons';

const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks; 

const {
	setAttributes,
} = wp.element;

const { 
	InnerBlocks,
} = wp.blockEditor;

const {
	
} = wp.components;


registerBlockType( 'uk-gb/uk-grid-item', {

	title: __( 'UI Kit Grid Item' ), // Block title.
	icon, 								// Block icon
	category: 'uk-gb-blocks', 				// Block category
	keywords: [ 						// Search by these keywords
		__( 'uk', 'UK', 'uikit' ),
	],
	parent: [ 'uk-gb/uk-grid-block' ],

	attributes: {
		isCard: {
			type: "boolean",
			default: false,
		},
		cardClass: {
			type: "string",
			default: false,
		}
	},

	edit: function( props ) {


		// Setup the attributes.
		const {
			attributes: {
				isCard,
				cardClass
			},
			isSelected,
			editable,
			className,
			setAttributes
		} = props;
		var ukMarginClasses = marginClasses(props.attributes);
		var ukPaddingClasses 	= paddingClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);

		var ukHeightClasses 	= heightClasses(props.attributes);

		return [
			
			<div className={`uk-gb-block`}>
				<div
					className={`uk-grid-item-inner-wrap uk-position-relative ${ukHeightClasses && ukHeightClasses} ${ukPaddingClasses && ukPaddingClasses} ${ukBoxShadowClasses && ukBoxShadowClasses}`}
					style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	

				>
					
					{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
					{props.isSelected  ? 
						<InnerBlocks 
							renderAppender={ () => (
								<InnerBlocks.ButtonBlockAppender />
							) }
						/> : 
						<InnerBlocks
							renderAppender={ false}
						/>
					}
				</div>
			</div>		
			
		];
	},

	save: function( props ) {

		const {
			attributes: {
				isCard,
				cardClass
			},
		} = props;
		var ukMarginClasses 	= marginClasses(props.attributes);
		var ukPaddingClasses 	= paddingClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);

		var ukHeightClasses 	= heightClasses(props.attributes);

		return (
			<div className={`uk-gb-block uk-grid-item ${ukMarginClasses && ukMarginClasses} `}>
				<div
					className={`uk-grid-item-inner-wrap uk-position-relative ${ukHeightClasses && ukHeightClasses} ${ukPaddingClasses && ukPaddingClasses} ${ukBoxShadowClasses && ukBoxShadowClasses}`}
					style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	
					uk-height-viewport={props.attributes.ukHeightDesk === 'uk-min-height-viewport' ? "true": false }
				>
					{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
					<div className="uk-position-relative">
						<InnerBlocks.Content />
					</div>
				</div>
			</div>
		);
	}
} );
