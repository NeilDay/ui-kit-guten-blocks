import classnames from 'classnames';

export default function marginClasses( attributes ) {

	var ukMarginClasses = [];

	var marginAttributes = [
		'ukBasicMargin',
		'ukTopMargin',
		'ukBottomMargin',
		'ukLeftMargin',
		'ukRightMargin',
		'ukMarginRemove',
		'ukMarginRemoveTop',
		'ukMarginRemoveBottom',
		'ukMarginRemoveLeft',
		'ukMarginRemoveRight',
		'ukMarginRemoveVertical',
		'ukMarginRemoveHorizontal',
	] 

	const hasMarginAttribute = marginAttributes.some(attributeName => attributes[attributeName]);

	// If we have bg atts - add the bg + width
	if (hasMarginAttribute) {
		// Width classes
		ukMarginClasses = classnames({
			[attributes.ukBasicMargin] : attributes.ukBasicMargin,
			[attributes.ukTopMargin] : attributes.ukTopMargin,
			[attributes.ukBottomMargin] : attributes.ukBottomMargin,
			[attributes.ukLeftMargin] : attributes.ukLeftMargin,
			[attributes.ukRightMargin] : attributes.ukRightMargin,
			'uk-margin-remove' : attributes.ukMarginTop,
			'uk-margin-remove-top' : attributes.ukMarginRemoveTop,
			'uk-margin-remove-bottom' : attributes.ukMarginRemoveBottom,
			'uk-margin-remove-left' : attributes.ukMarginRemoveLeft,
			'uk-margin-remove-right' : attributes.ukMarginRemoveRight,
			'uk-margin-remove-vertical' : attributes.ukMarginRemoveVertical,
			'uk-margin-remove-horizontal' : attributes.ukMarginRemoveHorizontal,
		})

	} else {
		
		ukMarginClasses = '';

	}


	return ukMarginClasses;
}