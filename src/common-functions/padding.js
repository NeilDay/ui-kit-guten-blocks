import classnames from 'classnames';

export default function paddingClasses( attributes ) {

	var ukPaddingClasses = [];

	var paddingAttributes = [
		'ukPadding',
		'ukPaddingRemove',
		'ukPaddingRemoveTop',
		'ukPaddingRemoveBottom',
		'ukPaddingRemoveLeft',
		'ukPaddingRemoveRight',
		'ukPaddingRemoveVertical',
		'ukPaddingRemoveHorizontal',
	] 

	const hasPaddingAttribute = paddingAttributes.some(attributeName => attributes[attributeName]);

	// If we have bg atts - add the bg + width
	if (hasPaddingAttribute) {
		// Width classes
		ukPaddingClasses = classnames({
			[attributes.ukPadding] : attributes.ukPadding,
			'uk-padding-remove' : attributes.ukPaddingRemove,
			'uk-padding-remove-top' : attributes.ukPaddingRemoveTop,
			'uk-padding-remove-bottom' : attributes.ukPaddingRemoveBottom,
			'uk-padding-remove-left' : attributes.ukPaddingRemoveLeft,
			'uk-padding-remove-right' : attributes.ukPaddingRemoveRight,
			'uk-padding-remove-vertical' : attributes.ukPaddingRemoveVertical,
			'uk-padding-remove-horizontal' : attributes.ukPaddingRemoveHorizontal,
		})

	} else {
		
		ukPaddingClasses = '';

	}


	return ukPaddingClasses;
}