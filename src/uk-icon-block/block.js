import map from 'lodash/map';

import classnames from 'classnames';


import marginClasses from '../common-functions/margin';
import UkIcon from '../components/icon';
import HorizontalAlignmentButtons from '../common-inspector-controls/horizontal_align';


//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

import UkIcons from '../uk-icons'

const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks; 

const {
	setAttributes,
} = wp.element;

const { 
	
	InspectorControls, 
	URLInput,
} = wp.blockEditor;

const {
	TextControl,
	SelectControl,
	ToggleControl,
    PanelBody,
    PanelRow,
	Dashicon
} = wp.components;

const ukButtonStyleOptions = [

	{label : 'default', 	value: 'uk-button-default'},
	{label : 'primary', 	value: 'uk-button-primary'},
	{label : 'secondary', 	value: 'uk-button-secondary'},

]


registerBlockType( 'uk-gb/uk-icon', {

	title: __( 'UI Kit Icon' ), // Block title.
	icon, 								// Block icon
	category: 'uk-gb-blocks', 				// Block category
	keywords: [ 						// Search by these keywords
		__( 'uk', 'UK', 'uikit' ),
	],
	supports: {
		className: false,
	},
	
	attributes: {

		icon: {
			type: 'string',
			default: ''
		},
		iconSize: {
			type: 'string',
			default: '1'
		},
		isButton: {
			type: 'bool',
			default: false
		},
		buttonStyle: {
			type: 'string',
			default: 'uk-button-default'
		},
		isLink: {
			type: 'bool',
			default: false
		},
		horizontalAlignment: {
			type: 'string',
			default: 'uk-text-left'
		},
		url: {
			type: 'string',
			default: ''
		},
		target: {
			type: 'bool',
			default: false
		},

	},

	edit: function( props ) {


		// Setup the attributes.
		const {
			attributes: {
				icon,
				iconSize,
				isButton,
				buttonStyle,
				isLink,
				horizontalAlignment,
				url,
				target
			},
			isSelected,
			className,
			setAttributes
		} = props;

		var ukMarginClasses = marginClasses(props.attributes);

		const deskHalignItems = [ 
	  		{ id: 1, value: 'uk-text-left', icon:<Dashicon icon="align-left" />},
  			{ id: 2, value: 'uk-text-center', icon:<Dashicon icon="align-center" />},
  			{ id: 3, value: 'uk-text-right', icon:<Dashicon icon="align-right" />}
	  	];
		
		const wrapperClasses = classnames( {
			[ props.attributes.horizontalAlignment ]: props.attributes.horizontalAlignment,
		} )

	  	const iconClasses = classnames( {
			'uk-icon-button uk-icon-ratio' : props.attributes.isButton === true,
			[ props.attributes.buttonStyle]: props.attributes.isButton === true
		} )

		return [
			<InspectorControls>
				<PanelBody
					title={ __( 'Icon Options' ) }
					initialOpen={ false }
				>	
					<h2 className={'uk-gb-section-heading'}>Alignment</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<HorizontalAlignmentButtons
							horizontalAlignment={props.attributes.horizontalAlignment}
							items={deskHalignItems}
							onClick={ ( value ) => props.setAttributes({ horizontalAlignment: value })}
						/>
					</div>
					<h2 className={'uk-gb-section-heading'}>Icon Size</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<TextControl
					        label={ __( 'Button Size' ) }
					        type={'number'}
					        value={ props.attributes.iconSize }
					        onChange={ ( value ) => {
								props.setAttributes( {
									iconSize: value,
								} );
							} }
					    />
					</div>
					<h2 className={'uk-gb-section-heading'}>Icon Button</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<ToggleControl
							label={__("Icon Button?")}
							checked={ !! props.attributes.isButton}
							onChange={() => {
								props.setAttributes({ isButton: !isButton });
							}}
						/>
						{props.attributes.isButton === true && 
							<SelectControl
								label={ __( 'Icon Button Style' ) }
								value={ props.attributes.buttonStyle }
								options={ ukButtonStyleOptions }
								onChange={ ( value ) => {
									props.setAttributes( {
										buttonStyle: value,
									} );
								} }
							/>
						}
					</div>
					<h2 className={'uk-gb-section-heading'}>Link</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<ToggleControl
							label={__("Make a link?")}
							checked={ !! props.attributes.isLink}
							onChange={() => {
								props.setAttributes({ isLink: !isLink });
							}}
						/>
					</div>
					
				</PanelBody>
				<PanelBody
					title={ __( 'Choose Icon' ) }
					initialOpen={ true }
				>	
					<h2 className={'uk-gb-section-heading'}>App</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<div className={'uk-gb-icon-select uk-grid uk-grid-small uk-child-width-1-5'}
							uk-grid=""
							>
							{UkIcons.UkAppIcons.map((icon) => {

						        return <UkIcon
										selectedIcon={props.attributes.icon}
										icon={icon.value}
										onClick={ ( value ) => {
											props.setAttributes( {
												icon: value,
											} );
										} }

									/>
						    })}
						</div>
					</div>
					<h2 className={'uk-gb-section-heading'}>Devices</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<div className={'uk-gb-icon-select uk-grid uk-grid-small uk-child-width-1-5'}
							uk-grid=""
							>
							{UkIcons.UkDevicesIcons.map((icon) => {

						        return <UkIcon
										selectedIcon={props.attributes.icon}
										icon={icon.value}
										onClick={ ( value ) => {
											props.setAttributes( {
												icon: value,
											} );
										} }

									/>
						    })}
						</div>
					</div>
					<h2 className={'uk-gb-section-heading'}>Storage</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<div className={'uk-gb-icon-select uk-grid uk-grid-small uk-child-width-1-5'}
							uk-grid=""
							>
							{UkIcons.UkStorageIcons.map((icon) => {

						        return <UkIcon
										selectedIcon={props.attributes.icon}
										icon={icon.value}
										onClick={ ( value ) => {
											props.setAttributes( {
												icon: value,
											} );
										} }

									/>
						    })}
						</div>
					</div>
					<h2 className={'uk-gb-section-heading'}>Direction</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<div className={'uk-gb-icon-select uk-grid uk-grid-small uk-child-width-1-5'}
							uk-grid=""
							>
							{UkIcons.UkDirectionIcons.map((icon) => {

						        return <UkIcon
										selectedIcon={props.attributes.icon}
										icon={icon.value}
										onClick={ ( value ) => {
											props.setAttributes( {
												icon: value,
											} );
										} }

									/>
						    })}
						</div>
					</div>
					<h2 className={'uk-gb-section-heading'}>Editor</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<div className={'uk-gb-icon-select uk-grid uk-grid-small uk-child-width-1-5'}
							uk-grid=""
							>
							{UkIcons.UkEditorIcons.map((icon) => {

						        return <UkIcon
										selectedIcon={props.attributes.icon}
										icon={icon.value}
										onClick={ ( value ) => {
											props.setAttributes( {
												icon: value,
											} );
										} }

									/>
						    })}
						</div>
					</div>
					<h2 className={'uk-gb-section-heading'}>Brands</h2>
					<div className={'editor-styles-wrapper uk-gb-panel-wrapper'}>
						<div className={'uk-gb-icon-select uk-grid uk-grid-small uk-child-width-1-5'}
							uk-grid=""
							>
							{UkIcons.UkBrandsIcons.map((icon) => {

						        return <UkIcon
										selectedIcon={props.attributes.icon}
										icon={icon.value}
										onClick={ ( value ) => {
											props.setAttributes( {
												icon: value,
											} );
										} }

									/>
						    })}
						</div>
					</div>
				</PanelBody>
			</InspectorControls>,
			<div
				className={ wrapperClasses }>
				<span 
					className={iconClasses}
					uk-icon={'icon: '+props.attributes.icon+'; ratio: '+props.attributes.iconSize}
				></span>
				{props.attributes.isLink === true && 
					<URLInput
						label={ __( 'Link' ) }
						className="wp-block-button__inline-link"
						value={ props.attributes.url }
						autoFocus={ false }
						onChange={ ( value ) => props.setAttributes( { url: value } ) }
						isFullWidth
						hasBorder
					/>
				}
			</div>
		];
	},

	save: function( props ) {

		const {
			attributes: {
				icon,
				iconSize,
				isButton,
				buttonStyle,
				isLink,
			},
		} = props;

		const wrapperClasses = classnames( {
			[ props.attributes.horizontalAlignment ]: props.attributes.horizontalAlignment,
		} )

	  	const iconClasses = classnames( {
			'uk-icon-button uk-icon-ratio' : props.attributes.isButton === true,
			[ props.attributes.buttonStyle]: props.attributes.isButton === true
		} )

		if (props.attributes.isLink === true ) {

			return (
				<div
					className={ wrapperClasses }>
					<a href={props.attributes.url} 
						className={iconClasses}
						uk-icon={'icon: '+props.attributes.icon+'; ratio: '+props.attributes.iconSize}
					></a>
				</div>
			)

		} else {

			return (
			
				<div
					className={ wrapperClasses }>
					<span 
						className={iconClasses}
						uk-icon={'icon: '+props.attributes.icon+'; ratio: '+props.attributes.iconSize}
					></span>
				</div>

			)
		}

	},

} );
