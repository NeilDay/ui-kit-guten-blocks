const UkIcons = {};

UkIcons.UkAppIcons = [
    
    {label: 'home', value: 'home'},
    {label: 'sign-in', value: 'sign-in'},
    {label: 'sign-out', value: 'sign-out'},
    {label: 'user', value: 'user'},
    {label: 'users', value: 'users'},
    {label: 'lock', value: 'lock'},
    {label: 'unlock', value: 'unlock'},
    {label: 'settings', value: 'settings'},
    {label: 'cog', value: 'cog'},
    {label: 'nut', value: 'nut'},
    {label: 'comment', value: 'comment'},
    {label: 'commenting', value: 'commenting'},
    {label: 'comments', value: 'comments'},
    {label: 'hashtag', value: 'hashtag'},
    {label: 'tag', value: 'tag'},
    {label: 'cart', value: 'cart'},
    {label: 'credit-card', value: 'credit-card'},
    {label: 'mail', value: 'mail'},
    {label: 'receiver', value: 'receiver'},
    {label: 'print', value: 'print'},
    {label: 'search', value: 'search'},
    {label: 'location', value: 'location'},
    {label: 'bookmark', value: 'bookmark'},
    {label: 'code', value: 'code'},
    {label: 'paint-bucket', value: 'paint-bucket'},
    {label: 'camera', value: 'camera'},
    {label: 'video-camera', value: 'video-camera'},
    {label: 'bell', value: 'bell'},
    {label: 'microphone', value: 'microphone'},
    {label: 'bolt', value: 'bolt'},
    {label: 'star', value: 'star'},
    {label: 'heart', value: 'heart'},
    {label: 'happy', value: 'happy'},
    {label: 'lifesaver', value: 'lifesaver'},
    {label: 'rss', value: 'rss'},
    {label: 'social', value: 'social'},
    {label: 'git-branch', value: 'git-branch'},
    {label: 'git-fork', value: 'git-fork'},
    {label: 'world', value: 'world'},
    {label: 'calendar', value: 'calendar'},
    {label: 'clock', value: 'clock'},
    {label: 'history', value: 'history'},
    {label: 'future', value: 'future'},
    {label: 'pencil', value: 'pencil'},
    {label: 'trash', value: 'trash'},
    {label: 'move', value: 'move'},
    {label: 'link', value: 'link'},
    {label: 'question', value: 'question'},
    {label: 'info', value: 'info'},
    {label: 'warning', value: 'warning'},
    {label: 'image', value: 'image'},
    {label: 'thumbnails', value: 'thumbnails'},
    {label: 'table', value: 'table'},
    {label: 'list', value: 'list'},
    {label: 'menu', value: 'menu'},
    {label: 'grid', value: 'grid'},
    {label: 'more', value: 'more'},
    {label: 'more-vertical', value: 'more-vertical'},
    {label: 'plus', value: 'plus'},
    {label: 'plus-circle', value: 'plus-circle'},
    {label: 'minus', value: 'minus'},
    {label: 'minus-circle', value: 'minus-circle'},
    {label: 'close', value: 'close'},
    {label: 'check', value: 'check'},
    {label: 'ban', value: 'ban'},
    {label: 'refresh', value: 'refresh'},
    {label: 'play', value: 'play'},
    {label: 'play-circle', value: 'play-circle'},
    
] 


UkIcons.UkDevicesIcons = [
    
    {label: 'tv', value: 'tv'},
    {label: 'desktop', value: 'desktop'},
    {label: 'laptop', value: 'laptop'},
    {label: 'tablet', value: 'tablet'},
    {label: 'phone', value: 'phone'},
    {label: 'tablet-landscape', value: 'tablet-landscape'},
    {label: 'phone-landscape', value: 'phone-landscape'},

]


UkIcons.UkStorageIcons = [
    
    {label: 'file', value: 'file'},
    {label: 'file-text', value: 'file-text'},
    {label: 'file-pdf', value: 'file-pdf'},
    {label: 'copy', value: 'copy'},
    {label: 'file-edit', value: 'file-edit'},
    {label: 'folder', value: 'folder'},
    {label: 'album', value: 'album'},
    {label: 'push', value: 'push'},
    {label: 'pull', value: 'pull'},
    {label: 'server', value: 'server'},
    {label: 'database', value: 'database'},
    {label: 'cloud-upload', value: 'cloud-upload'},
    {label: 'cloud-download', value: 'cloud-download'},
    {label: 'download', value: 'download'},
    {label: 'upload', value: 'upload'},

]
    
UkIcons.UkDirectionIcons = [
    
    {label: 'reply', value: 'reply'},
    {label: 'forward', value: 'forward'},
    {label: 'expand', value: 'expand'},
    {label: 'shrink', value: 'shrink'},
    {label: 'arrow-up', value: 'arrow-up'},
    {label: 'arrow-down', value: 'arrow-down'},
    {label: 'arrow-left', value: 'arrow-left'},
    {label: 'arrow-right', value: 'arrow-right'},
    {label: 'chevron-up', value: 'chevron-up'},
    {label: 'chevron-down', value: 'chevron-down'},
    {label: 'chevron-left', value: 'chevron-left'},
    {label: 'chevron-right', value: 'chevron-right'},
    {label: 'chevron-double-left', value: 'chevron-double-left'},
    {label: 'chevron-double-right', value: 'chevron-double-right'},
    {label: 'triangle-up', value: 'triangle-up'},
    {label: 'triangle-down', value: 'triangle-down'},
    {label: 'triangle-left', value: 'triangle-left'},
    {label: 'triangle-right', value: 'triangle-right'},

]

UkIcons.UkEditorIcons = [
    
    {label: 'bold', value: 'bold'},
    {label: 'italic', value: 'italic'},
    {label: 'strikethrough', value: 'strikethrough'},
    {label: 'quote-right', value: 'quote-right'},

]

UkIcons.UkBrandsIcons = [
    
    {label: 'behance', value: 'behance'},
    {label: 'dribbble', value: 'dribbble'},
    {label: 'etsy', value: 'etsy'},
    {label: 'facebook', value: 'facebook'},
    {label: 'flickr', value: 'flickr'},
    {label: 'foursquare', value: 'foursquare'},
    {label: 'github', value: 'github'},
    {label: 'github-alt', value: 'github-alt'},
    {label: 'gitter', value: 'gitter'},
    {label: 'google', value: 'google'},
    {label: 'google-plus', value: 'google-plus'},
    {label: 'instagram', value: 'instagram'},
    {label: 'joomla', value: 'joomla'},
    {label: 'linkedin', value: 'linkedin'},
    {label: 'pagekit', value: 'pagekit'},
    {label: 'pinterest', value: 'pinterest'},
    {label: 'reddit', value: 'reddit'},
    {label: 'soundcloud', value: 'soundcloud'},
    {label: 'tripadvisor', value: 'tripadvisor'},
    {label: 'tumblr', value: 'tumblr'},
    {label: 'twitter', value: 'twitter'},
    {label: 'uikit', value: 'uikit'},
    {label: 'vimeo', value: 'vimeo'},
    {label: 'whatsapp', value: 'whatsapp'},
    {label: 'wordpress', value: 'wordpress'},
    {label: 'xing', value: 'xing'},
    {label: 'yelp', value: 'yelp'},
    {label: 'youtube', value: 'youtube'},

]

    


export default UkIcons;