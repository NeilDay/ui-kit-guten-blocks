/**
 * BLOCK: Gutenberg Repeater Field
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

// Import icon
import icon from '../icon';

const {
	__,
} = wp.i18n;
const {
	registerBlockType,
} = wp.blocks;
const {
	Button,
	IconButton,
	PanelBody,
	TextControl,
} = wp.components;
const {
	InspectorControls,
	InnerBlocks,
} = wp.blockEditor;
const {
	Fragment,
} = wp.element;

/**
 * Register: Repeater Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'uk-gb/ui-kit-tabs', {
	title: __( 'UI Kit Tabs' ),
	icon: icon,
	category: 'uk-gb-blocks',
	attributes: {
		locations: {
			type: 'array',
			default: [],
		},
	},
	keywords: [
		__( 'Gutenberg Repeater Field' ),
		__( 'Repeatable' ),
		__( 'ACF' ),
	],
	edit: ( props ) => {
		const handleAddLocation = () => {
			const locations = [ ...props.attributes.locations ];
			locations.push( {
				address: '',
			} );
			props.setAttributes( { locations } );
		};

		const handleRemoveLocation = ( index ) => {
			const locations = [ ...props.attributes.locations ];
			locations.splice( index, 1 );
			props.setAttributes( { locations } );
		};

		const handleLocationChange = ( address, index ) => {
			const locations = [ ...props.attributes.locations ];
			locations[ index ].address = address;
			props.setAttributes( { locations } );
		};

		let locationFields,
			locationDisplay;

		if ( props.attributes.locations.length ) {
			locationFields = props.attributes.locations.map( ( location, index ) => {
				return <Fragment key={ index }>
					<TextControl
						className="grf__location-address"
						placeholder="Tab Title"
						value={ props.attributes.locations[ index ].address }
						onChange={ ( address ) => handleLocationChange( address, index ) }
					/>
					<IconButton
						className="grf__remove-location-address"
						icon="no-alt"
						label="Delete tab"
						onClick={ () => handleRemoveLocation( index ) }
					/>
				</Fragment>;
			} );

			locationDisplay = props.attributes.locations.map( ( location, index ) => {
				return <li key={ index }><a href="#">{ location.address }</a></li>;
			} );
		}

		return [
			<InspectorControls key="1">
				<PanelBody title={ __( 'Tabs' ) }>
					{ locationFields }
					<Button
						isDefault
						onClick={ handleAddLocation.bind( this ) }
					>
						{ __( 'Add Tab' ) }
					</Button>
				</PanelBody>
			</InspectorControls>,
			<div>
				<ul key="2" className={ `wp-block ${props.className}` }
					uk-tab="">
						{ locationDisplay }
				</ul>
				<div class="uk-switcher">
				    <InnerBlocks 
						renderAppender={ () => (
							<InnerBlocks.ButtonBlockAppender />
							
						) }

					/>
				</div>
			</div>
			,
		];
	},
	save: ( props ) => {
		const locationFields = props.attributes.locations.map( ( location, index ) => {
			return <li key={ index }><a href="#">{ location.address }</a></li>;
		} );

		return (
			<div>
				<ul className={ props.className+' uk-margin-remove-bottom' }
					uk-tab="">
						{ locationFields }
				</ul>
				<div class="uk-switcher">
			    	<InnerBlocks.Content />
				</div>
			</div>
		);
	},
} );