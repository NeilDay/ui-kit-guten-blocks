import map from 'lodash/map';

import classNames from 'classnames';

import marginClasses from '../common-functions/margin';

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

import icons from '../icons';

const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks; 

const {
	setAttributes,
	Component,
	Fragment,
} = wp.element;

const { 
	InspectorControls, 
	InnerBlocks,
	RichText,
	URLInput,
} = wp.blockEditor;

const {
	TextControl,
	SelectControl,
	ToggleControl,
    PanelBody,
    PanelRow,
} = wp.components;

const ukButtonStyleOptions = [
	{label : 'default', 	value: 'uk-button-default'},
	{label : 'primary', 	value: 'uk-button-primary'},
	{label : 'secondary', 	value: 'uk-button-secondary'},
	{label : 'danger', 		value: 'uk-button-danger'},
	{label : 'text', 		value: 'uk-button-text'},
	{label : 'link', 		value: 'uk-button-link'},
]

const ukButtonSizeOptions = [
	{label : 'default', 	value: ''},
	{label : 'small', 		value: 'uk-button-small'},
	{label : 'large', 		value: 'uk-button-large'},
]


registerBlockType( 'uk-gb/uk-button', {

	title: __( 'UI Kit Button' ), // Block title.
	icon, 								// Block icon
	category: 'uk-gb-blocks', 				// Block category
	keywords: [ 						// Search by these keywords
		__( 'uk', 'UK', 'uikit' ),
	],
	supports: {
		align: [ 'left', 'center', 'right'],
		className: true,
	},

	attributes: {
		buttonText: {
			type: 'string',
			default: 'Button'
		},
		url: {
			type: 'string',
			default: ''
		},
		ukButtonStyle: {
			type: 'string',
			default: ''
		},
		ukButtonSize: {
			type: 'string',
			default: ''
		},
		target: {
			type: 'bool',
			default: false
		},
		isModal: {
			type: 'bool',
			default: false
		}
	},

	edit: function( props ) {
		// Setup the attributes.
		const {
			attributes: {
				buttonText,
				url,
				ukButtonStyle,
				ukButtonSize,
				target,
				isModal
			},
			isSelected,
			className,
			setAttributes
		} = props;

		var ukMarginClasses = marginClasses(props.attributes);

		return [
			<InspectorControls>
				<PanelBody
					title={ __( 'UI Kit Button Options' ) }
					initialOpen={ true }
				>
					<SelectControl
						label={ __( 'Button Style' ) }
						value={ props.attributes.ukButtonStyle }
						options={ ukButtonStyleOptions }
						onChange={ ( value ) => {
							props.setAttributes( {
								ukButtonStyle: value,
							} );
						} }
					/>
					<SelectControl
						label={ __( 'Button Size' ) }
						value={ props.attributes.ukButtonSize }
						options={ ukButtonSizeOptions }
						onChange={ ( value ) => {
							props.setAttributes( {
								ukButtonSize: value,
							} );
						} }
					/>
					<ToggleControl
						label={__("Open in new tab")}
						checked={ !! props.attributes.target}
						onChange={() => {
							props.setAttributes({ target: !target });
						}}
					/>
					<ToggleControl
						label={__("Trigger a Modal")}
						checked={ !! props.attributes.isModal}
						onChange={() => {
							props.setAttributes({ isModal: !isModal });
						}}
					/>
				</PanelBody>
			</InspectorControls>,
			<Fragment>
				<button className={`uk-button ${props.attributes.ukButtonStyle} ${props.attributes.ukButtonSize} ${ukMarginClasses && ukMarginClasses}`}>
					<RichText
						tagName="span"
						multiline={false}
						placeholder={ __( 'Button', 'uk-accordion-item-block') }
						onChange={value => props.setAttributes({ buttonText: value })}
						value={ props.attributes.buttonText }
						withoutInteractiveFormatting
						formattingControls={ [ ] }
					/>
				</button>	
				{props.isSelected &&
					<URLInput
						label={ __( 'Link' ) }
						className="wp-block-button__inline-link"
						value={ props.attributes.url }
						autoFocus={ false }
						onChange={ ( value ) => props.setAttributes( { url: value } ) }
						isFullWidth
						hasBorder
					/>
				}				
					
			</Fragment>
			
		];
	},

	save: function( props ) {
		const {
			attributes: {
				buttonText,
				url,
				ukButtonStyle,
				ukButtonSize,
				target,
				isModal
			},

		} = props;

		var ukMarginClasses = marginClasses(props.attributes);
		
		const wrapperClasses = classNames(
  			'uk-gb-button-wrapper',
  			ukMarginClasses,
  			{
    			'uk-display-block uk-text-center': props.attributes.align === 'center',
  			}
		);

		if (props.attributes.align === 'center') {
			return (
				<div className={`${ wrapperClasses &&  wrapperClasses}`}>
					<a className={`uk-button ${props.attributes.ukButtonStyle} ${props.attributes.ukButtonSize}`}
						href={props.attributes.url}
						{...props.attributes.target && {'target' : '_BLANK'}}
						{...props.attributes.target && {'rel' : 'noopener noreferrer'}}
						{...props.attributes.isModal === true && {'uk-toggle' : ''}}
					>
						{props.attributes.buttonText}
					</a>
				</div>
			);
			
		} else {
			return (
				<a className={`uk-button  ${ wrapperClasses &&  wrapperClasses} ${props.attributes.ukButtonStyle} ${props.attributes.ukButtonSize}`}
					href={props.attributes.url}
					{...props.attributes.target && {'target' : '_BLANK'}}
					{...props.attributes.target && {'rel' : 'noopener noreferrer'}}
					{...props.attributes.isModal === true && {'uk-toggle' : ''}}
				>
					{props.attributes.buttonText}
				</a>
			);
		}
	},
} );
