

//  Import CSS.
import './style.scss';
import './editor.scss';

import icon from '../icon'
const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks;
const { InspectorControls, InnerBlocks, RichText } = wp.blockEditor;
const {
    PanelBody,
    PanelRow,
	RadioControl
} = wp.components;


registerBlockType( 'uk-gb/uk-accordion-item-block', {
	title: __( 'UI Kit Accordion Item' ),
	icon, 
	category: 'uk-gb-blocks',
	keywords: [
		__( 'uk', 'UK' ),
		__( 'Test' ),
		__( 'accordion', 'container' ),
	],
	parent: [ 'uk-gb/uk-accordion-container-block' ],
	attributes: {
		isOpen: {
			default: '',
			isOpen: ''
		},
		title: {
			type: 'string',
			multiline: 'h2',
			selector: '.uk-card-title',
		},

		visibility_verb: {
			default: '',
			visibility_verb: ''
		},
		visibility_size: {
			default: '',
			visibility_size: ''
		},
		visibility_class: {
			default: '',
			visibility_class: ''
		},
		width_x: {
			default: '',
			width_x: ''
		},
		width_y: {
			default: '',
			width_y: ''
		},
		width_class: {
			default: '',
			width_class: ''
		},
		marginRadio: {
			default: '',
			marginRadio: ''
		},
		textAlign: {
			default: '',
			textAlign: ''
		}
	},

	edit: function( props ) {

		const onChangeTitle = newTitle => props.setAttributes({title: newTitle});

		return [
			<InspectorControls>
				<PanelBody
					title={ __( 'Accordion Settings', 'uk-accordion-item-block' ) }
				>
					<PanelRow>
						<RadioControl 
							label='Do you want this item to be open?'
							options={[
									{ label: 'No', value: '' },
									{ label: 'Yes', value: 'uk-open' },
								]}
							onChange={( value ) => {
								props.setAttributes( { isOpen: value } );
							}}							
							selected={props.attributes.isOpen}
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>,
			<div className={ 'uk-accordion-item' }>
				<div class="uk-accordion-title">	
					<RichText
						tagName="div"
						multiline="h2"
						placeholder={ __( 'Title', 'uk-accordion-item-block') }
						onChange={ onChangeTitle }
						value={ props.attributes.title }
					/>
				</div>
				<div class="uk-accordion-content">
		            <InnerBlocks 
			        	renderAppender={ () => (
			        		<InnerBlocks.ButtonBlockAppender />
			        	) }

			        />
		        </div>
					
			</div>
		];
	},

	save: function( props ) {
		return (
			<li className={`${props.attributes.isOpen} ${props.attributes.textAlign} ${props.attributes.marginRadio} ${props.attributes.width_class} ${props.attributes.visibility_class}`}>
				<a class="uk-accordion-title" href="#">{props.attributes.title}</a>
				<div class="uk-accordion-content">
					<InnerBlocks.Content />
				</div>
			</li>
		);
	},
} );
