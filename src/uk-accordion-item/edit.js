/**
 * BLOCK: Kadence Column
 *
 * Registering a basic block with Gutenberg.
 */

import $ from "jquery";

import map from 'lodash/map';

//import preview from './preview';
/**
 * Import Icons
 */

 const { select, withSelect, withDispatch } = wp.data;
 const { compose,ifCondition, withState } = wp.compose;
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;

const {
	Component,
	Fragment,
} = wp.element;
const {
	InnerBlocks,
	MediaUpload,
	InspectorControls,
	BlockControls,
	AlignmentToolbar,
} = wp.blockEditor;
const {
	TabPanel,
	Dashicon,
	PanelBody,
	PanelRow,
	TextControl,
	Button,
	ButtonGroup,
	IconButton,
	SelectControl,
	Tooltip,
	RangeControl,
	Toolbar,
} = wp.components;

//JQuery for slider init
var wrapSlider = function(){

	var r = $.Deferred();
	
	$('.uk-slideshow-preview> .block-editor-inner-blocks > .block-editor-block-list__layout').contents().not('.block-list-appender').wrapAll('<div class="uk-accordion"></div>');

	return r;
}

var initSlider = function(){

	var r = $.Deferred();
	
	UIkit.accordion('.uk-accordion');

	return r;
}

class Preview extends Component {

	componentDidMount() {

		wrapSlider().done( initSlider() );

	}

	
	
	render (){
		return (

	    	<div class="uk-accordion-preview">
				<InnerBlocks 
					templateLock="all"
					renderAppender={ () => (
						<InnerBlocks.ButtonBlockAppender />
					) }

				/>
	    		
	    	</div>
		)

	}
}

/**
 * Build the spacer edit
 */
class Accordion extends Component {

	constructor() {

	  	super();
	  	this.state = {

	    	editorState: 'previewing',

	  	};
	}

	render() {
		const { 

			attributes: { 
				
			}
		} = this.props;


	    const items = [ 
	  		{ id: 1, value: 'editing', icon:<Dashicon icon="edit" /> },
	  		{ id: 2, value: 'previewing', icon:<Dashicon icon="visibility" /> },
	  	];

	  	const sliderId = this.props.clientId;
		
		return [
			<BlockControls key="controls">
				<Toolbar>
					<Tooltip text={ __( 'Thick Border', 'jsforwpblocks' )  }>
                        <IconButton
                        	className="uk-gb-editor-toggle components-toolbar__control"
		                    isToggled={ this.state.editorState === 'editing' }
							onClick={ () => 
								{
									this.setState({ editorState: 'editing' })
								}
							}
						>
							{ <Dashicon icon="edit" /> }
						</IconButton>
                        <IconButton
                        	className="uk-gb-editor-toggle components-toolbar__control"
                        	isActive={ this.state.editorState === 'previewing' }
		                    isToggled={ this.state.editorState === 'previewing' }
							onClick={ () => 
								{
									this.setState({ editorState: 'previewing' })
								}
							}
						>
							{ <Dashicon icon="visibility" /> }
						</IconButton>
                    </Tooltip>
                </Toolbar>
			</BlockControls>,
			<div class="accordion-container">

				{
					this.state.editorState === 'editing' ? (
					
						<div class="uk-accordion-editor">
					        <InnerBlocks 
					        	renderAppender={ () => (
					        		<InnerBlocks.ButtonBlockAppender />
					        	) }

					        />
					    </div>
						
			        	
			        ) : 
			        	<Preview/>
					
		    	}
					
			</div>
		];
	}
}
export default compose( [

] )( Accordion );
