import map from 'lodash/map';

import classnames from 'classnames';

import Background from '../components/background';

import icons from '../icons';

import paddingClasses from '../common-functions/padding';
import marginClasses from '../common-functions/margin';
import boxShadowClasses from '../common-functions/box-shadow';
import heightClasses from '../common-functions/height';
import VerticalAlignmentButtons from '../common-inspector-controls/vertical_align';

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

const { __ } = wp.i18n;

const { registerBlockType } = wp.blocks;

const {
	setAttributes, 
} = wp.element;

const { 
	InspectorControls,
	InnerBlocks,
} = wp.blockEditor;

const { 
	PanelBody,
	PanelRow,
	ToggleControl
} = wp.components;

const deskValignItems = [ 
	{ id: 1, value: 'uk-flex-stretch', icon:icons.alignStretch},
	{ id: 1, value: 'uk-flex-top', icon:icons.alignTop},
	{ id: 2, value: 'uk-flex-middle', icon:icons.alignCenter},
	{ id: 3, value: 'uk-flex-bottom', icon:icons.alignBottom}
];

registerBlockType( 'uk-gb/uk-item-block', {
	
	title: __( 'UI Kit Section' ), 
	icon, 
	category: 'uk-gb-blocks', 
	keywords: [
		__( 'uk', 'UK' ),
		__( 'container' ),
	],
	supports: {
		align: ['center', 'left', 'right', 'wide', 'full' ],
		anchor: false,
		customClassName: true,
		className: true,
		html: true,
		inserter: true,
		multiple: true,
		reusable: true
	},
    
	attributes: {// Atts required for bg

		verticalAlignment: {
			type: 'string',
			default: 'uk-flex-top'
		},

		ukContainContent: {
			type: 'bool',
			default: true
		}
	},
	
	edit: function( props ) {	

		const {
			attributes: {
				verticalAlignment,
				ukContainContent,
			},
			className
		} = props;

		var ukPaddingClasses = paddingClasses(props.attributes);
		var ukMarginClasses = marginClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);
		var ukHeightClasses 	= heightClasses(props.attributes);

		var classes = [
			ukPaddingClasses,
			ukMarginClasses,
			ukBoxShadowClasses,
			ukHeightClasses
		];

		var wrapperClasses  = classnames({
			'uk-gb-contained' : ukContainContent},
			classes
		)

		return [
			<InspectorControls>
				<PanelBody
					title={ __( 'Content Alignment' ) }
					initialOpen={ true }
				>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<h2>Vertical Alignment</h2>
						<VerticalAlignmentButtons
							verticalAlignment={props.attributes.verticalAlignment}
							items={deskValignItems}
							onClick={ ( value ) => props.setAttributes({ verticalAlignment: value })}
						/>
					</div>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<h2>Contain content to UI Kit max-width</h2>
						<div>
							<ToggleControl
								label={__("")}
								checked={ !! props.attributes.ukContainContent}
								onChange={() => {
									props.setAttributes({ ukContainContent: !ukContainContent });
								}}
							/>
						</div>
					</div>
					<PanelRow>
						
					</PanelRow>
				</PanelBody>
			</InspectorControls>,
			<div className={`uk-gb-block uk-item-block uk-flex ${props.attributes.verticalAlignment} ${wrapperClasses}`}
				style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	
				>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				
				{props.isSelected  ? 
					<InnerBlocks
						renderAppender={ () => (
							<InnerBlocks.ButtonBlockAppender />
						) }
					/> : 
					<InnerBlocks
						renderAppender={ false}
					/>
				}

			</div>	
				
		];
	},

	save: function( props ) {

		const {
			attributes: {
				verticalAlignment,
				ukContainContent,
			},
			className
		} = props;

		var ukPaddingClasses = paddingClasses(props.attributes);
		var ukMarginClasses = marginClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);
		var ukHeightClasses 	= heightClasses(props.attributes);

		var classes = [
			ukPaddingClasses,
			ukMarginClasses,
			ukBoxShadowClasses,
			ukHeightClasses
		];

		var wrapperClasses  = classnames({
			'uk-gb-contained' : ukContainContent},
			classes
		)

		return (
			<div className={`wp-block uk-gb-block uk-item-block uk-flex ${props.attributes.verticalAlignment} ${wrapperClasses}`}
				style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	
				>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				<div className={'uk-gb-block-inner-wrapper'}>
					<InnerBlocks.Content />
				</div>
			</div>
				
		);
	},
} );
