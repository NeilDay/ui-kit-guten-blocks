/**
 * Animation Component
 *
 */

/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;
const {
	Component,
} = wp.element;
const {
	PanelBody,
	SelectControl,
	ToggleControl
} = wp.components;

// Animation Options
const ukMarginBasicOptions = [
	{label: 'none', 			value: ''},
	{label: 'default', 			value: 'uk-margin'},
	{label: 'small', 			value: 'uk-margin-small'},
	{label: 'large', 			value: 'uk-margin-large'},
	{label: 'xlarge', 			value: 'uk-margin-xlarge'},
];

const ukMarginTopOptions = [
	{label: 'none', 			value: ''},
	{label: 'default', 			value: 'uk-margin-top'},
	{label: 'small', 			value: 'uk-margin-small-top'},
	{label: 'medium', 			value: 'uk-margin-medium-top'},
	{label: 'large', 			value: 'uk-margin-large-top'},
	{label: 'xlarge', 			value: 'uk-margin-xlarge-top'},
];

const ukMarginBottomOptions = [
	{label: 'none', 			value: ''},
	{label: 'default', 			value: 'uk-margin-bottom'},
	{label: 'small', 			value: 'uk-margin-small-bottom'},
	{label: 'medium', 			value: 'uk-margin-medium-bottom'},
	{label: 'large', 			value: 'uk-margin-large-bottom'},
	{label: 'xlarge', 			value: 'uk-margin-xlarge-bottom'},
];

const ukMarginLeftOptions = [
	{label: 'none', 			value: ''},
	{label: 'default', 			value: 'uk-margin-left'},
	{label: 'small', 			value: 'uk-margin-small-left'},
	{label: 'medium', 			value: 'uk-margin-medium-left'},
	{label: 'large', 			value: 'uk-margin-large-left'},
	{label: 'xlarge', 			value: 'uk-margin-xlarge-left'},
];

const ukMarginRightOptions = [
	{label: 'none', 			value: ''},
	{label: 'default', 			value: 'uk-margin-right'},
	{label: 'small', 			value: 'uk-margin-small-right'},
	{label: 'medium', 			value: 'uk-margin-medium-right'},
	{label: 'large', 			value: 'uk-margin-large-right'},
	{label: 'xlarge', 			value: 'uk-margin-xlarge-right'},
];



/**
 * Build the Animation controls
 * @returns {object} Animation settings.
 */
class UkMarginControl extends Component {

	render() {
		return (

			<PanelBody
				title={ __( 'UK Margin' ) }
	            initialOpen={ false }
			>
				<SelectControl
					label={ __( 'Basic Margin' ) }
					value={ this.props.ukBasicMargin }
					options={ ukMarginBasicOptions }
					onChange={ ( value ) => {
						this.props.onChange('ukBasicMargin',value)
					} }
				/>
				<SelectControl
					label={ __( 'Top Margin' ) }
					value={ this.props.ukTopMargin }
					options={ ukMarginTopOptions }
					onChange={ ( value ) => {
						this.props.onChange('ukTopMargin',value)
					} }
				/>
				<SelectControl
					label={ __( 'Bottom Margin' ) }
					value={ this.props.ukBottomMargin }
					options={ ukMarginBottomOptions }
					onChange={ ( value ) => {
						this.props.onChange('ukBottomMargin',value)
					} }
				/>
				<SelectControl
					label={ __( 'Left Margin' ) }
					value={ this.props.ukLeftMargin }
					options={ ukMarginLeftOptions }
					onChange={ ( value ) => {
						this.props.onChange('ukLeftMargin',value)
					} }
				/>
				<SelectControl
					label={ __( 'Right Margin' ) }
					value={ this.props.ukRightMargin }
					options={ ukMarginRightOptions }
					onChange={ ( value ) => {
						this.props.onChange('ukRightMargin',value)
					} }
				/>

			</PanelBody>
		);
	}
}
export default UkMarginControl;
