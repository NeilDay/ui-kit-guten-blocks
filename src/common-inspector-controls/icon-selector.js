/**
 * Width Control Component
 *
 */

/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;

const {
	Component,
	Fragment
} = wp.element;

const {
	PanelBody,
	PanelRow,
	TabPanel,
	SelectControl,
	Dashicon
} = wp.components;

/**
 * Build the Animation controls
 * @returns {object} Animation settings.
 */
class UkWidthControl extends Component {
	// Width Options
	

	render() {

		const ukWidthDeskOptions = [
			{ label: 'none', value: '' },
			{ label: '1-1', value: 'uk-width-1-1@m' },
			{ label: '1-2', value: 'uk-width-1-2@m' },
			{ label: '1-3', value: 'uk-width-1-3@m' },
			{ label: '2-3', value: 'uk-width-2-3@m' },
			{ label: '1-4', value: 'uk-width-1-4@m' },
			{ label: '3-4', value: 'uk-width-3-4@m' },
			{ label: '1-5', value: 'uk-width-1-5@m' },
			{ label: '2-5', value: 'uk-width-2-5@m' },
			{ label: '3-5', value: 'uk-width-3-5@m' },
			{ label: '4-5', value: 'uk-width-4-5@m' },
			{ label: '1-6', value: 'uk-width-1-6@m' },
			{ label: '5-6', value: 'uk-width-5-6@m' },
		];

		const ukWidthTabOptions = [
			{ label: 'none', value: '' },
			{ label: '1-1', value: 'uk-width-1-1@s' },
			{ label: '1-2', value: 'uk-width-1-2@s' },
			{ label: '1-3', value: 'uk-width-1-3@s' },
			{ label: '2-3', value: 'uk-width-2-3@s' },
			{ label: '1-4', value: 'uk-width-1-4@s' },
			{ label: '3-4', value: 'uk-width-3-4@s' },
			{ label: '1-5', value: 'uk-width-1-5@s' },
			{ label: '2-5', value: 'uk-width-2-5@s' },
			{ label: '3-5', value: 'uk-width-3-5@s' },
			{ label: '4-5', value: 'uk-width-4-5@s' },
			{ label: '1-6', value: 'uk-width-1-6@s' },
			{ label: '5-6', value: 'uk-width-5-6@s' },
		];

		const ukWidthMobOptions = [
			{ label: 'none', value: '' },
			{ label: '1-1', value: 'uk-width-1-1' },
			{ label: '1-2', value: 'uk-width-1-2' },
			{ label: '1-3', value: 'uk-width-1-3' },
			{ label: '2-3', value: 'uk-width-2-3' },
			{ label: '1-4', value: 'uk-width-1-4' },
			{ label: '3-4', value: 'uk-width-3-4' },
			{ label: '1-5', value: 'uk-width-1-5' },
			{ label: '2-5', value: 'uk-width-2-5' },
			{ label: '3-5', value: 'uk-width-3-5' },
			{ label: '4-5', value: 'uk-width-4-5' },
			{ label: '1-6', value: 'uk-width-1-6' },
			{ label: '5-6', value: 'uk-width-5-6' },
		];

		/*if (!this.props.gridItem){
			ukWidthDeskOptions.push({ label: 'Full Width', value: 'uk-width-full' })
			ukWidthTabOptions.push({ label: 'Full Width', value: 'uk-width-full' })
			ukWidthMobOptions.push({ label: 'Full Width', value: 'uk-width-full' })
		} */

		return (

			<PanelBody
				title={ __( 'UK Width', 'uk-gb' ) }
				initialOpen={ false }
			>
				<TabPanel className="uk-gb-tab-panel uk-gb-responsive-settings"
					activeClass="active-tab"
					tabs={ [
						{
							name: 'desktop',
							title: <Dashicon icon="desktop" />,
							className: 'uk-gb-tab uk-gb-desktop-tab',
						},
						{
							name: 'tablet',
							title: <Dashicon icon="tablet" />,
							className: 'uk-gb-tab uk-gb-tablet-tab',
						},
						{
							name: 'mobile',
							title: <Dashicon icon="smartphone" />,
							className: 'uk-gb-tab uk-gb-mobile-tab',
						},
					] }>
					{
						( tab ) => {
							let tabout;
							if ( tab.name ) {
								if ( 'desktop' === tab.name ) {
									tabout = (
										<Fragment>
											<SelectControl
												label={ __( 'Desktop Width' ) }
												value={ this.props.ukDeskWidth }
												options={ ukWidthDeskOptions }
												onChange={ ( value ) => {
													this.props.onChange('ukDeskWidth',value)
												} }
											/>
										</Fragment>
									);
								} 
								if ( 'tablet' === tab.name ) {
									tabout = (
										<Fragment>
											<SelectControl
												label={ __( 'Tablet Width' ) }
												value={ this.props.ukTabWidth }
												options={ ukWidthTabOptions }
												onChange={ ( value ) => {
													this.props.onChange('ukTabWidth',value)
												} }
											/>
										</Fragment>
									);
								} 
								if ( 'mobile' === tab.name ) {
									tabout = (
										<Fragment>
											<SelectControl
												label={ __( 'Mobile Width' ) }
												value={ this.props.ukMobWidth }
												options={ ukWidthMobOptions }
												onChange={ ( value ) => {
													this.props.onChange('ukMobWidth',value)
												} }
											/>
										</Fragment>
									);
								} 
							}
							return <div>{ tabout }</div>;
						}
					}
				</TabPanel>
			</PanelBody>
		);
	}
}
export default UkWidthControl;
