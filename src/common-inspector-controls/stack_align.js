import React from 'react';

import map from 'lodash/map';

import icons from '../icons';

const { __ } = wp.i18n; // Import __() from wp.i18n

const {
	createElement,
	forwardRef,
	setAttributes,
} = wp.element;

// Import Inspector components
const {
	Button,
	ButtonGroup,
	Dashicon
} = wp.components;


export function StackAlignmentButtons( {
	stackAlignment,
	onClick

} ) {
    const items = [ 
  		{ id: 1, value: 'uk-flex-row', icon:<Dashicon icon="grid-view" /> },
  		{ id: 2, value: 'uk-flex-column', icon:<Dashicon icon="editor-justify" /> },
  	];

   	return (
      	<ButtonGroup
			className="uk-gb-button-group"
      	>
			{ map( items, ( { id, value, icon } ) => (
				<Button
					key={ id }
					isDefault
					isPrimary={ stackAlignment === value }
                    isToggled={ stackAlignment === value }
					onClick={ () => onClick( value ) }
				>
					{ icon }
				</Button>
			) ) }
      	</ButtonGroup>
   	);
}

export default forwardRef( StackAlignmentButtons );
  
  