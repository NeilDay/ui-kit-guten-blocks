/**
 * Animation Component
 *
 */

/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;
const {
	Component,
} = wp.element;
const {
	PanelBody,
	SelectControl,
	ToggleControl
} = wp.components;

// Animation Options
const ukPaddingOptions = [
	{label: 'none', 			value: ''},
	{label: 'default', 			value: 'uk-padding'},
	{label: 'small', 			value: 'uk-padding-small'},
	{label: 'large', 			value: 'uk-padding-large'},
];

const ukPaddingRemoveOptions = [
	{label: 'remove', 				value: 'uk-padding-remove'},
	{label: 'remove-top', 			value: 'uk-padding-remove-top'},
	{label: 'remove-bottom', 		value: 'uk-padding-remove-bottom'},
	{label: 'remove-left', 			value: 'uk-padding-remove-left'},
	{label: 'remove-right', 		value: 'uk-padding-remove-right'},
	{label: 'remove-vertical', 		value: 'uk-padding-remove-vertical'},
	{label: 'remove-horizontal', 	value: 'uk-padding-remove-horizontal'},
];

/**
 * Build the Animation controls
 * @returns {object} Animation settings.
 */
class UkPaddingControl extends Component {

	render() {
		return (

			<PanelBody
				title={ __( 'UK Padding' ) }
	            initialOpen={ false }
			>
				<SelectControl
					label={ __( 'Padding' ) }
					value={ this.props.ukPadding }
					options={ ukPaddingOptions }
					onChange={ ( value ) => {
						this.props.onChange('ukPadding',value)
					} }
				/>
				<ToggleControl
					label={__("Remove ALL Padding")}
					checked={ !! this.props.ukPaddingRemove}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemove',value)
					} }
				/>
				<ToggleControl
					label={__("Remove TOP Padding")}
					checked={ !! this.props.ukPaddingRemoveTop}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemoveTop',value)
					} }
				/>
				<ToggleControl
					label={__("Remove BOTTOM Padding")}
					checked={ !! this.props.ukPaddingRemoveBottom}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemoveBottom',value)
					} }
				/>
				<ToggleControl
					label={__("Remove LEFT Padding")}
					checked={ !! this.props.ukPaddingRemoveLeft}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemoveLeft',value)
					} }
				/>
				<ToggleControl
					label={__("Remove RIGHT Padding")}
					checked={ !! this.props.ukPaddingRemoveRight}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemoveRight',value)
					} }
				/>
				<ToggleControl
					label={__("Remove VERTICAL Padding")}
					checked={ !! this.props.ukPaddingRemoveVertical}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemoveVertical',value)
					} }
				/>
				<ToggleControl
					label={__("Remove HORIZONTAL Padding")}
					checked={ !! this.props.ukPaddingRemoveHorizontal}
					onChange={ ( value ) => {
						this.props.onChange('ukPaddingRemoveHorizontal',value)
					} }
				/>

			</PanelBody>
		);
	}
}
export default UkPaddingControl;
