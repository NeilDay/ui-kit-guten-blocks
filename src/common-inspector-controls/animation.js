/**
 * Animation Component
 *
 */

/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;
const {
	Component,
} = wp.element;
const {
	PanelBody,
	RangeControl,
	SelectControl
} = wp.components;
const {
	withSelect,
} = wp.data;

// Animation Options
const ukAnimationOptions = [
	{label: 'none', 				value: 'none'},
	{label: 'scale-up', 			value: 'uk-animation-scale-up'},
	{label: 'scale-down', 			value: 'uk-animation-scale-down'},
	{label: 'slide-top',			value: 'uk-animation-slide-top'},
	{label: 'slide-bottom', 		value: 'uk-animation-slide-bottom'},
	{label: 'slide-left', 			value: 'uk-animation-slide-left'},
	{label: 'slide-right', 			value: 'uk-animation-slide-right'},
	{label: 'slide-top-small', 		value: 'uk-animation-slide-top-small'},
	{label: 'slide-bottom-small', 	value: 'uk-animation-slide-bottom-small'},
	{label: 'slide-left-small', 	value: 'uk-animation-slide-left-small'},
	{label: 'slide-right-small', 	value: 'uk-animation-slide-right-small'},
	{label: 'slide-top-medium', 	value: 'uk-animation-slide-top-medium'},
	{label: 'slide-bottom-medium', 	value: 'uk-animation-slide-bottom-medium'},
	{label: 'slide-left-medium', 	value: 'uk-animation-slide-left-medium'},
	{label: 'slide-right-medium', 	value: 'uk-animation-slide-right-medium'},
	{label: 'kenburns', 			value: 'uk-animation-kenburns'},
	{label: 'shake', 				value: 'uk-animation-shake'},
	{label: 'stroke', 				value: 'uk-animation-stroke'},
];

/**
 * Build the Animation controls
 * @returns {object} Animation settings.
 */
class UkAnimationControl extends Component {

	render() {
		return (

			<PanelBody
				title={ __( 'UK Animations Control' ) }
	            initialOpen={ false }
			>
				<SelectControl
					label={ __( 'Animation Type' ) }
					value={ this.props.animationType }
					options={ ukAnimationOptions }
					onChange={ ( value ) => {
						this.props.onChange('animationType',value)
					} }
				/>
				<RangeControl
					label={ __( 'Animation Delay' ) }
					value={ this.props.animationDelay }
					min={0}
					max={5000}
					step={100}
					onChange={ ( value ) => {
						this.props.onChange('animationDelay',value)
					} }
				/>
				<RangeControl
					label={ __( 'Animation Offset' ) }
					value={ this.props.animationOffset }
					min={ -1000 }
					max={ 1000 }
					step={ 100 }
					onChange={ ( value ) => {
						this.props.onChange('animationOffset',value)
					} }
				/>
			</PanelBody>
		);
	}
}
export default UkAnimationControl;
