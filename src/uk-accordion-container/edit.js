/**
 * BLOCK: Kadence Column
 *
 * Registering a basic block with Gutenberg.
 */

import $ from "jquery";

import map from 'lodash/map';

//import preview from './preview';
/**
 * Import Icons
 */

 const { select, withSelect, withDispatch } = wp.data;
 const { compose,ifCondition, withState } = wp.compose;
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;

const {
	Component,
	Fragment,
} = wp.element;
const {
	InnerBlocks,
	MediaUpload,
	InspectorControls,
	BlockControls,
	AlignmentToolbar,
} = wp.blockEditor;
const {
	TabPanel,
	Dashicon,
	PanelBody,
	PanelRow,
	TextControl,
	Button,
	ButtonGroup,
	IconButton,
	SelectControl,
	RadioControl,
	Tooltip,
	RangeControl,
	Toolbar,
} = wp.components;

const ALLOWED_BLOCKS = [ 'cgb/uk-accordion-item-block' ];

//JQuery for slider init
var getAccordionItems = function(id){

	
	
	var r = $.Deferred();

	const accordionIdAttribute = 'accordion-'+id;

	

	var accordionPreview = $('#accordion-preview-'+id);

	var accordionHolder = $('#accordion-holder-'+id);

	var accordionItems = $(accordionPreview).find(' > .block-editor-inner-blocks > .block-editor-block-list__layout > .wp-block[data-type="cgb/uk-accordion-item-block"] > .block-editor-block-list__block-edit > [data-block] > .uk-accordion-item');

	$(accordionHolder).html(accordionItems);


	return r;
}

var initAccordion = function(id){
	
	
	var r = $.Deferred();
	
	var accordionHolder = $('#accordion-holder-'+id);
	
	UIkit.accordion('#accordion-holder-'+id+'.uk-accordion-holder');

	return r;
}

class AccordionPreview extends Component {



	componentDidMount() {

		getAccordionItems(this.props.accordionId).done( initAccordion(this.props.accordionId) );

	}
	
	
	
	render (){
		return (

	    	<div id={"accordion-preview-"+this.props.accordionId} class="uk-accordion-preview">
	    		<div id={"accordion-holder-"+this.props.accordionId} class="uk-accordion-holder"></div>

				<InnerBlocks 
					allowedBlocks={ALLOWED_BLOCKS}
					templateLock="all"
					renderAppender={ () => (
						<InnerBlocks.ButtonBlockAppender />
					) }

				/>
	    		
	    	</div>
		)

	}
}

/**
 * Build the edit component
 */
class Accordion extends Component {



	constructor() {

	  	super();
	  	this.state = {

	    	editorState: 'previewing',

	  	};
	}

	componentDidMount() {

		const select = wp.data.select('core/editor');
	    const selected = select.getBlockSelectionStart();
	    const inner = select.getBlock(this.props.clientId).innerBlocks;
	    if (!inner.length){
	    	this.setState({ editorState: 'editing' });
	    }

	}

	

	render() {
		const { 

			attributes: {
				accordionSetting
			},

		} = this.props;

		

		const { clientId } = this.props

	  	const accordionId = this.props.clientId;

	  	const accordionIdAttribute = 'accordion-'+accordionId;
		
		return [
			<BlockControls key="controls">
				<Toolbar>
					<Tooltip text={ __( 'Thick Border', 'jsforwpblocks' )  }>
                        <IconButton
                        	className="uk-gb-editor-toggle components-toolbar__control"
		                    isToggled={ this.state.editorState === 'editing' }
							onClick={ () => 
								{
									this.setState({ editorState: 'editing' })
								}
							}
						>
							{ <Dashicon icon="edit" /> }
						</IconButton>
                        <IconButton
                        	className="uk-gb-editor-toggle components-toolbar__control"
                        	isActive={ this.state.editorState === 'previewing' }
		                    isToggled={ this.state.editorState === 'previewing' }
							onClick={ () => 
								{
									this.setState({ editorState: 'previewing' })
								}
							}
						>
							{ <Dashicon icon="visibility" /> }
						</IconButton>
                    </Tooltip>
                </Toolbar>
			</BlockControls>,
			<InspectorControls>
				<PanelBody
					title={ __( 'Accordion Settings', 'uk-accordion-container-block' ) }
				>
					<PanelRow>
						<RadioControl 
							label='Pick an accordion setting (descriptions on UIkit website)'
							options={[
									{ label: 'None', value: '' },
									{ label: 'No collapsing', value: 'collapsible: false' },
									{ label: 'Muptiple open items', value: 'multiple: true' },
								]}
							onChange={( value ) => {
								this.props.setAttributes( { accordionSetting: value } );
							
							}}							
							
							selected={this.props.attributes.accordionSetting}
							
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>,
			<div class="uk-accordion-editor">
				<div>
				{
					this.state.editorState === 'editing' ? (
						<div id={accordionIdAttribute} class="accordion-item">
							<p className={`directions`}>Place uk accordion item blocks in here.</p>
							<InnerBlocks 
								allowedBlocks={ALLOWED_BLOCKS}
								accordionSetting={this.props.accordionSetting}
							/>
						</div>
						
			        	
			        ) : 
			        	<AccordionPreview
							accordionId={accordionId} 
			        	/>
					
		    	}
		    	</div>
					
			</div>
						
		];
	}
}
export default compose( [

] )( Accordion );
