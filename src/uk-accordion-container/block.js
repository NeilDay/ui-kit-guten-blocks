//  Import CSS.
import './style.scss';
import './editor.scss';

//Import icon
import icon from '../icon';

import edit from './edit';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;
const {
	
} = wp.components;

// Allowed Children
const ALLOWED_BLOCKS = [ 'uk-gb/uk-accordion-item-block' ];

registerBlockType( 'uk-gb/uk-accordion-container-block', {

	title: __( 'UI Kit Accordion Block' ), 
	icon,
	category: 'uk-gb-blocks',
	keywords: [
		__( 'uk', 'UK' ),
		__( 'Test' ),
		__( 'accordion', 'container' ),
	],
	attributes: {
		accordionSetting: {
			default: '',
			accordionSetting: ''
		},
	},

	edit,

	save: function( props ) {
		return (
			<div className={props.attributes.className} uk-accordion={`${props.attributes.accordionSetting}`}>
				<InnerBlocks.Content />
			</div>
		);
	},
} );
