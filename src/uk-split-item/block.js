import map from 'lodash/map';

import classnames from 'classnames';

import Background from '../components/background';

import icons from '../icons';

import paddingClasses from '../common-functions/padding';
import marginClasses from '../common-functions/margin';
import boxShadowClasses from '../common-functions/box-shadow';
import heightClasses from '../common-functions/height';
import VerticalAlignmentButtons from '../common-inspector-controls/vertical_align';

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

const { __ } = wp.i18n;

const { registerBlockType } = wp.blocks;

const {
	setAttributes, 
} = wp.element;

const { 
	InnerBlocks,
	InspectorControls, 
} = wp.blockEditor;

const { 
	PanelBody,
    ToggleControl,
} = wp.components;

const deskValignItems = [ 
	{ id: 1, value: 'uk-flex-stretch', icon:icons.alignStretch},
	{ id: 1, value: 'uk-flex-top', icon:icons.alignTop},
	{ id: 2, value: 'uk-flex-middle', icon:icons.alignCenter},
	{ id: 3, value: 'uk-flex-bottom', icon:icons.alignBottom}
];


registerBlockType( 'uk-gb/uk-split-item', {
	
	title: __( 'UI Kit Split Item' ), 
	icon, 
	category: 'uk-gb-blocks', 
	keywords: [
		__( 'uk', 'UK' ),
		__( 'container' ),
	],
	parent: [ 'uk-gb/uk-split-bg' ],
	supports: {
		anchor: false,
		customClassName: true,
		className: true,
		html: true,
		inserter: true,
		multiple: true,
		reusable: true
	},
    
	attributes: {// Atts required for bg
		
		containToGrid : {
			type: 'bool',
			default: 'true'
		},
		verticalAlignment: {
			type: 'string',
			default: 'uk-flex-top'
		},

	},
	
	edit: function( props ) {	

		var ukPaddingClasses = paddingClasses(props.attributes);
		var ukMarginClasses = marginClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);
		var ukHeightClasses 	= heightClasses(props.attributes);

		const {
			attributes: {
				containToGrid,
				verticalAlignment
			},
			isSelected,
			editable,
			className,
			setAttributes
		} = props;


		return [
			<InspectorControls>
				<PanelBody
					title={ __( 'Item Settings' ) }
					initialOpen={ true }
				>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<ToggleControl
							label={__("Contain the item to the page width")}
							checked={!!props.attributes.containToGrid}
							onChange={() => {
								props.setAttributes({ containToGrid: !containToGrid });
							}}
						/>
					</div>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<p>Content Vertical Alignment</p>
						<VerticalAlignmentButtons
							verticalAlignment={props.attributes.verticalAlignment}
							items={deskValignItems}
							onClick={ ( value ) => props.setAttributes({ verticalAlignment: value })}
						/>
					</div>
				</PanelBody>
			</InspectorControls>,
			<div className={`uk-gb-block uk-split-item-edit ${props.attributes.containToGrid && 'uk-split-item-contained uk-position-relative'} ${ukMarginClasses && ukMarginClasses}`}
				>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				<div className={`uk-position-relative`}>
					<div className={`split-item-inner uk-flex ${props.attributes.verticalAlignment} ${ukHeightClasses && ukHeightClasses} ${ukPaddingClasses && ukPaddingClasses} ${ukBoxShadowClasses && ukBoxShadowClasses}`}
						style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}
						>

						{props.isSelected  ? 
							<InnerBlocks 
								renderAppender={ () => (
									<InnerBlocks.ButtonBlockAppender />
								) }
							/> : 
							<InnerBlocks
								renderAppender={ false}
							/>
						}
					</div>
				</div>
			</div>
				
		];
	},

	save: function( props ) {
		var ukPaddingClasses = paddingClasses(props.attributes);
		var ukMarginClasses = marginClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);
		var ukHeightClasses 	= heightClasses(props.attributes);

		const {
			attributes: {
				containToGrid,
				verticalAlignment
			},
			className,
		} = props;

		return (
			<div className={`uk-gb-block uk-split-item ${props.attributes.containToGrid && 'uk-split-item-contained uk-position-relative'} ${ukMarginClasses && ukMarginClasses}`}
				style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	
				uk-height-viewport={props.attributes.ukHeightDesk === 'uk-min-height-viewport' ? 'true': false }
				>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				<div className={`uk-position-relative`}>
					<div className={`split-item-inner uk-flex ${props.attributes.verticalAlignment} ${ukHeightClasses && ukHeightClasses} ${ukPaddingClasses && ukPaddingClasses} ${ukBoxShadowClasses && ukBoxShadowClasses}`}>
						<InnerBlocks.Content />
					</div>
				</div>
			</div>
				
		);
	},
} );
