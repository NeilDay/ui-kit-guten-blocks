<?php
/**
 * The template part for displaying modals.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Mend a Bath
 */

//Modal ID
$modal_id 		= get_field( 'modal_id');

//Class names
$modal_classes		= '';
$dialog_classes 	= '';
$close_classes 		= 'uk-modal-close-default';

//Modal Atts
$modal_atts 		= '';
$modal_dialog_atts 	= '';

//Full Screen?
if (get_field( 'full_screen' ) ){
	$modal_classes 		.= ' uk-modal-full';
	$close_classes 		.= ' uk-modal-close-full uk-close-large';
	$modal_body_atts 	.= ' uk-height-viewport'; 
};

//Overflow?
if (get_field( 'scroll_overflow' ) ){
	$modal_dialog_atts .= ' uk-overflow-auto';
};

//Center?
if (get_field( 'center_modal_vertically' ) ){
	$modal_classes .= ' uk-flex-top';
	$dialog_classes .= ' uk-margin-auto-vertical';
};

?>

<div id="<?php echo $modal_id;?>" class="<?php echo $modal_classes;?>" uk-modal=""<?php echo $modal_atts;?>>
    <div class="uk-modal-dialog<?php echo $dialog_classes;?>"<?php echo $modal_dialog_atts;?>>
        <button class="<?php echo $close_classes;?>" type="button" uk-close=""></button>

        <?php if( get_field( 'show_title_in_header' ) ){ ?>
        	<div class="uk-modal-header">
	            <h2 class="uk-modal-title uk-text-center"><?php the_title();?></h2>
	        </div>
        <?php } ?>
	        
        <div class="uk-modal-body <?php if ( get_field( 'full_screen') ){ echo 'uk-padding-remove-vertical';}?>">
        	<?php the_content();?>
        </div>

        <?php if( get_field( 'show_footer' ) ){ ?>
        	<div class="uk-modal-footer uk-text-center">
	            <button class="uk-button uk-button-secondary uk-modal-close" type="button">Close</button>
	        </div>
        <?php } ?>
    </div>
</div>
