//Filters imported before blocks - IMPORTANT
import './block-filters/filters.js';

//Blocks
import './uk-grid-block/block.js';
import './uk-grid-item-block/block.js';
import './uk-item-block/block.js';
import './uk-accordion-container/block.js';
import './uk-accordion-item/block.js';
import './uk-button-block/block.js';
import './uk-divider-block/block.js';
import './uk-heading-block/block.js';
import './uk-icon-block/block.js';

//Dummy Block to imprt global editor styles
import './dummy-styles-block/block.js';